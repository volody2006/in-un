from django import forms
from django.utils.translation import ugettext as _



class TimerForm(forms.Form):
    structure_choices = [('POCO', _('POCO')), ('I-HUB', _('I-HUB')), ('POS[S]', _('POS[S]')),
                         ('POS[M]', _('POS[M]')), ('POS[L]', _('POS[L]')),
                         ('Station', _('Station')), ('Other', _('Other'))]
    fleet_type_choices = [('Armor', _('Armor')), ('Shield', _('Shield')), ('Other', _('Other'))]

    name = forms.CharField(max_length=254, required=True, label=_('Fleet Name'))
    system = forms.CharField(max_length=254, required=True, label=_('System'))
    planet_moon = forms.CharField(max_length=254, label=_('Planet/Moon'), required=False, initial='')
    structure = forms.ChoiceField(choices=structure_choices, required=True, label=_('Structure Type'))
    fleet_type = forms.ChoiceField(choices=fleet_type_choices, required=True, label=_('Fleet Type'))
    eve_time = forms.DateTimeField(required=True, label=_('Eve Time'))
    important = forms.BooleanField(label=_('Important'), required=False)