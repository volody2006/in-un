#!/usr/bin/env bash
ps ww | grep 'manage.py' | grep -v grep | awk '{print $1}' | xargs kill
#ps ww | grep 'redis-server' | grep -v grep | awk '{print $1}' | xargs kill
sudo supervisorctl restart all
#redis-3.0.3/src/redis-server &
# TODO route log output to file.
python manage.py makemigrations
python manage.py migrate

#python manage.py shell < run_alliance_corp_update.py

python manage.py celeryd --verbosity=2 --loglevel=INFO &
python manage.py celerybeat --verbosity=2 --loglevel=INFO &
#python manage.py runserver &

ps ww | grep 'redis-server' | grep -v grep | awk '{print $1}'
