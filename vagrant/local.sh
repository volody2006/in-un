#!/usr/bin/env bash
# -*- coding: utf-8 -*-
ps ww | grep 'manage.py' | grep -v grep | awk '{print $1}' | xargs kill
#ps ww | grep 'redis-server' | grep -v grep | awk '{print $1}' | xargs kill

python manage.py makemessages -l ru
python manage.py makemessages -l en
python manage.py makemessages -a
# Компиляция файлов с сообщениями
python manage.py compilemessages


python manage.py makemigrations

python manage.py migrate

#python manage.py shell < run_alliance_corp_update.py

#python manage.py celeryd --verbosity=2 --loglevel=INFO &
#python manage.py celerybeat --verbosity=2 --loglevel=INFO &
python manage.py runserver &

#ps ww | grep 'redis-server' | grep -v grep | awk '{print $1}'
