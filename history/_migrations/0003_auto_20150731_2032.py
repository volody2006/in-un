# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('history', '0002_alliancesovereignty'),
    ]

    operations = [
        migrations.AlterField(
            model_name='corporationalliance',
            name='alliance',
            field=models.ForeignKey(blank=True, to='eveonline.EveAllianceInfo', null=True),
        ),
    ]
