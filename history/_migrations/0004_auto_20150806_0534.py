# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('history', '0003_auto_20150731_2032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='corporationtax',
            name='tax',
            field=models.DecimalField(null=True, max_digits=28, decimal_places=2, blank=True),
        ),
    ]
