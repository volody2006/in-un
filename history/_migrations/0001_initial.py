# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0009_auto_20150729_0609'),
    ]

    operations = [
        migrations.CreateModel(
            name='CharacterCorp',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('character', models.ForeignKey(to='eveonline.EveCharacter')),
                ('corp', models.ForeignKey(to='eveonline.EveCorporationInfo')),
            ],
        ),
        migrations.CreateModel(
            name='CorporationAlliance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('alliance', models.ForeignKey(to='eveonline.EveAllianceInfo')),
                ('corp', models.ForeignKey(to='eveonline.EveCorporationInfo')),
            ],
        ),
        migrations.CreateModel(
            name='CorporationCeo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('ceo', models.ForeignKey(to='eveonline.EveCharacter')),
                ('corp', models.ForeignKey(to='eveonline.EveCorporationInfo')),
            ],
        ),
        migrations.CreateModel(
            name='CorporationDescription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('corp', models.ForeignKey(to='eveonline.EveCorporationInfo')),
            ],
        ),
        migrations.CreateModel(
            name='CorporationTax',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tax', models.IntegerField(null=True, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('corp', models.ForeignKey(to='eveonline.EveCorporationInfo')),
            ],
        ),
    ]
