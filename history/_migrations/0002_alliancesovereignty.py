# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0009_auto_20150729_0609'),
        ('history', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AllianceSovereignty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('alliance', models.ForeignKey(blank=True, to='eveonline.EveAllianceInfo', null=True)),
                ('corp', models.ForeignKey(blank=True, to='eveonline.EveCorporationInfo', null=True)),
                ('system', models.ForeignKey(to='eveonline.EveMapSovereignty')),
            ],
        ),
    ]
