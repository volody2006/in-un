# -*- coding: utf-8 -*-

__author__ = 'user'

from django.db import models
from django.contrib.auth.models import User
from django.utils.datetime_safe import datetime
from eveonline.models import EveCorporationInfo, EveMapSovereignty, EveCharacter, EveAllianceInfo

class CorporationTax(models.Model):
    tax = models.DecimalField(max_digits=28, decimal_places=2, blank=True, null=True)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)
    corp = models.ForeignKey(EveCorporationInfo)

class CorporationCeo(models.Model):
    ceo = models.ForeignKey(EveCharacter)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)
    corp = models.ForeignKey(EveCorporationInfo)

class CorporationAlliance(models.Model):
    alliance = models.ForeignKey(EveAllianceInfo, blank=True, null=True)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)
    corp = models.ForeignKey(EveCorporationInfo)


class CharacterCorp(models.Model):
    character = models.ForeignKey(EveCharacter)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)
    corp = models.ForeignKey(EveCorporationInfo)

class CorporationDescription(models.Model):
    description = models.TextField(blank=True, null=True)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)
    corp = models.ForeignKey(EveCorporationInfo)

class AllianceSovereignty(models.Model):
    system = models.ForeignKey(EveMapSovereignty)
    alliance = models.ForeignKey(EveAllianceInfo, blank=True, null=True)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)
    corp = models.ForeignKey(EveCorporationInfo, blank=True, null=True)