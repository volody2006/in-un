# -*- coding: utf-8 -*-
from datetime import timedelta
from alliance_auth import settings
from celery.task import periodic_task
from eveonline.managers import EveManager
from eveonline.models import EveMapSovereignty, EveAllianceInfo, EveCorporationInfo
from history.managers import history_system, history_corp_alliance
from evelink.map import Map
from services.managers.eve_api_manager import EveApiManager

__author__ = 'user'

#@periodic_task(run_every = timedelta(hours= 4))
def updateEveMapSovereignty():
    map = Map()
    maps = map.sov_by_system()
    for i in maps.result[0]:
        print maps.result[0][i]
        try:
            system = EveMapSovereignty.objects.get(system_id= i)
            alliance = EveAllianceInfo.objects.get(alliance_id=maps.result[0][i]['alliance_id'])
            corp = EveCorporationInfo.objects.get(corporation_id=maps.result[0][i]['corp_id'])
       #     history_system(system, alliance, corp)
        except:
            pass

        #if maps.result[0][i]['alliance_id'] != system.alliance_id:

         #   print ('Смена альянса в системе')


        obj, created = EveMapSovereignty.objects.get_or_create(
            system_id = maps.result[0][i]['id'],
            defaults=dict(
                alliance_id = maps.result[0][i]['alliance_id'],
                corp_id = maps.result[0][i]['corp_id'],
                system_name = maps.result[0][i]['name'],
                faction_id = maps.result[0][i]['faction_id']))


#@periodic_task(run_every = timedelta(hours = 3))
def run_history_corp_alliance():
    all_corp = EveManager.get_all_corporation_info()
    for corp in all_corp:
        alliance = corp.alliance
        history_corp_alliance(corp, alliance)
        print ('History corp %s in alliance %s' % (corp.corporation_name, alliance))

def run_tax_corp_update():
    # I am not proud of this block of code
    if EveApiManager.check_if_api_server_online():
        alliances_info = EveApiManager.get_all_alliance_information()
        alliance_info = alliances_info[int(settings.ALLIANCE_ID)]
        print ("Updated alliance info: %s" % (alliance_info['name']))
        # Populate alliance info
        if not EveManager.check_if_alliance_exists_by_id(settings.ALLIANCE_ID):
            EveManager.create_alliance_info(settings.ALLIANCE_ID, alliance_info['name'], alliance_info['ticker'],
                                            alliance_info['executor_id'], alliance_info['member_count'], False)
        alliance = EveManager.get_alliance_info_by_id(settings.ALLIANCE_ID)
        # Create the corps in the alliance
        for alliance_corp in alliance_info['member_corps']:
            corpinfo = EveApiManager.get_corporation_information(alliance_corp)
            corp = EveCorporationInfo.objects.get(corporation_id=corpinfo['id'])
            corp.tax = corpinfo['tax_percent']
            corp.save()

            print ("Corp: %s, Tax %s" % (corpinfo['name'], corpinfo['tax_percent'] ))

    print corpinfo

def run_tax_corp():
    corps = EveCorporationInfo.objects.all()
    for corp in corps:
        corpinfo = EveApiManager.get_corporation_information(corp.corporation_id)
        print corpinfo