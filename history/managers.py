# -*- coding: utf-8 -*-
from history.models import AllianceSovereignty, CorporationTax, CorporationCeo, CorporationAlliance, CharacterCorp, \
    CorporationDescription

__author__ = 'user'


def history_system(system, alliance, corp ):
    obj, created = AllianceSovereignty.objects.get_or_create(
            system = system,
            alliance = alliance,
            corp = corp)


def history_corp_tax(corp, tax):
    obj, created = CorporationTax.objects.get_or_create(
            corp = corp,
            tax = tax)


def history_corp_ceo(corp, character):
    obj, created = CorporationCeo.objects.get_or_create(
            corp = corp,
            ceo = character)


def history_corp_alliance(corp, alliance):
    obj, created = CorporationAlliance.objects.get_or_create(
            corp = corp,
            alliance = alliance)


def history_character_corp(corp, character):
    obj, created = CharacterCorp.objects.get_or_create(
            corp = corp,
            character = character)


def history_corp_description(corp, description):
    obj, created = CorporationDescription.objects.get_or_create(
            corp = corp,
            description = description)


