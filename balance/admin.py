# -*- coding: utf-8 -*-
from balance.models import Tariffs, TariffConnected
from django.contrib import admin
import reversion
# Register your models here.

class TariffsAdmin(reversion.VersionAdmin):
    list_filter = ['name' ]
    search_fields = ['name',
                     'note']
    list_display = ('name','static_amount', 'percentage', 'period', 'type')


class TariffConnectedAdmin(reversion.VersionAdmin):
    list_filter = ['tariff' ]
  #  search_fields = ['tariff__name', 'character__character_name',]
    list_display = ('tariff', 'character', 'user', 'date_start', 'date_stop')


admin.site.register(Tariffs, TariffsAdmin)
admin.site.register(TariffConnected, TariffConnectedAdmin)
