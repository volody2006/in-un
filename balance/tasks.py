# -*- coding: utf-8 -*-
import calendar

from celery.task import periodic_task
from datetime import timedelta, datetime
from django.utils import timezone
from django.utils.translation import ugettext as _
from alliance_auth import settings
from balance.managers import summa_tarif
from balance.models import Tariffs, TariffConnected
from eveonline.models import EveCharacter
from wallet.models import UserReportBounty

__author__ = 'user'

@periodic_task(run_every = timedelta(days = 1))
def on_tariff():
    tariffs  = Tariffs.objects.filter(is_active = False)
    for tariff in tariffs:
        if tariff.date_start is not None:
            date_now = timezone.now()
            print date_now, tariff.date_stop
            if tariff.date_start > date_now:
                tariff.is_active = True
                tariff.save()
    #Включаем тариф всем юзерам и чарам альянса
    all_char = EveCharacter.objects.filter(alliance_id = settings.ALLIANCE_ID)

    tariffs  = Tariffs.objects.filter(type = 1,
                                      is_active = True)
    for char in all_char:
        for tariff in tariffs:
            #Тариф включаем или с даты вступления в альянс, или с даты старта тарифа.
            #Если дата вступления позже влючения тарифа.
            if char.created > tariff.date_start:
                date = char.created
            else:
                date = tariff.date_start
            obj, created = TariffConnected.objects.update_or_create(
                character = char,
                tariff = tariff,
                defaults = dict(
                    date_start = date,
                    date_stop = tariff.date_stop,
                    user = char.user,
                )
            )
        # Считаем сколько должен заплатить чар по всем тарифам, добавляем запись в ЮзерРепорт
        for report in UserReportBounty.objects.filter(character=char):
            # Добавить условия по дате
            report.amount_tax = summa_tarif(report)
            report.save()


@periodic_task(run_every = timedelta(minutes = 1))
def off_tariff():
    #Выключаем тариф, если дата тарифа окончания тарифа указана
    tariffs  = Tariffs.objects.filter(is_active = True)
    for tariff in tariffs:
        if tariff.date_stop is not None:
            date_now = timezone.now()
            print date_now, tariff.date_stop
            if tariff.date_stop < date_now:
                tariff.is_active = False
                tariff.save()
    #Удаляем тариф у чара
        for char in TariffConnected.objects.all():
            pass



def month_period1(period=None):
    to_date = datetime.now()
    if period == 0:
        from_date = to_date.replace(day=1, hour=0, minute=0, second=0)
        return from_date, to_date
    else:

        now_mon = to_date.month
        last_mon = now_mon - period
        year = to_date.year
        last_day = calendar.monthrange(year, last_mon)
        from_date = to_date.replace(month=last_mon, day=1, hour=0, minute=0, second=0)
        to_date = to_date.replace(month=last_mon, day=last_day[1], hour=23, minute=59, second=59)
        return from_date, to_date