# -*- coding: utf-8 -*-
from balance.models import Tariffs, TariffConnected
from wallet.models import UserReportBounty
from django.utils.translation import ugettext as _


def summa_tarif(report_obj):
 #   report_obj = UserReportBounty.objects.get(pk=1)
    char = report_obj.character

    tariffs = TariffConnected.objects.filter(character=char)
    summa = 0


    for tariff in tariffs:
        if tariff.tariff.is_active:
            if tariff.tariff.limit_bounty < report_obj.summa:
                summa = summa + (tariff.tariff.static_amount + (report_obj.summa -tariff.tariff.limit_bounty)*(tariff.tariff.percentage/100))
            else:
                summa = summa + tariff.tariff.static_amount



    print (u'Сумма: %s млн, Чар: %s, репорт: %s млн' % (int(summa/1000000), report_obj.character.character_name, int(report_obj.summa/1000000)))
    return summa