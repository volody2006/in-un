# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('eveonline', '0007_auto_20150813_0437'),
        ('balance', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TariffConnected',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('date_start', models.DateTimeField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u0442\u0430\u0440\u0442\u0430 \u0442\u0430\u0440\u0438\u0444\u0430', blank=True)),
                ('date_stop', models.DateTimeField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f \u0442\u0430\u0440\u0438\u0444\u0430', blank=True)),
                ('character', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.EveCharacter', null=True)),
                ('tariff', models.ForeignKey(to='balance.Tariffs')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
    ]
