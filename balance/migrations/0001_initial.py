# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('eveonline', '0007_auto_20150813_0437'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tariffs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=254, verbose_name='\u0418\u043c\u044f \u0442\u0430\u0440\u0438\u0444\u0430')),
                ('static_amount', models.DecimalField(default=0, verbose_name='\u0421\u0442\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u0430\u044f \u0441\u0443\u043c\u043c\u0430', max_digits=28, decimal_places=2)),
                ('percentage', models.DecimalField(default=0, verbose_name='\u043f\u0440\u043e\u0446\u0435\u043d\u0442\u044b', max_digits=28, decimal_places=4)),
                ('limit_bounty', models.DecimalField(default=0, verbose_name='\u0421\u0443\u043c\u043c\u0430 \u0437\u0430\u0440\u0430\u0431\u043e\u0442\u043a\u0430 \u0432\u043a\u043b\u044e\u0447\u0435\u043d\u043d\u043e\u0433\u043e \u0432 \u0441\u0442\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u0443\u044e \u0441\u0443\u043c\u043c\u0443', max_digits=28, decimal_places=2)),
                ('period', models.IntegerField(verbose_name='\u0417\u0430 \u043a\u0430\u043a\u043e\u0439 \u043f\u0435\u0440\u0438\u043e\u0434 \u0431\u0435\u0440\u0435\u0442\u0441\u044f \u0441\u0443\u043c\u043c\u0430', choices=[(1, 'year'), (2, 'month'), (3, 'week'), (4, 'day'), (5, 'hour')])),
                ('type', models.IntegerField(verbose_name='\u0422\u0438\u043f \u0442\u0430\u0440\u0438\u0444\u0430 (\u043f\u0438\u043b\u043e\u0442, \u043a\u043e\u0440\u043f\u043e\u0440\u0430\u0446\u0438\u044f, \u0430\u043b\u044c\u044f\u043d\u0441)', choices=[(1, 'character'), (2, 'corporation'), (3, 'alliance'), (4, '\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c')])),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('date_start', models.DateTimeField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u0442\u0430\u0440\u0442\u0430 \u0442\u0430\u0440\u0438\u0444\u0430', blank=True)),
                ('date_stop', models.DateTimeField(null=True, verbose_name='\u0414\u0430\u0442\u0430 \u043e\u043a\u043e\u043d\u0447\u0430\u043d\u0438\u044f \u0442\u0430\u0440\u0438\u0444\u0430', blank=True)),
                ('is_active', models.BooleanField(default=False, verbose_name='\u0422\u0430\u0440\u0438\u0444 \u0430\u043a\u0442\u0438\u0432\u0435\u043d?')),
                ('note', models.TextField(default=b'', verbose_name='\u041f\u0440\u0438\u043c\u0435\u0447\u0430\u043d\u0438\u0435', blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.DecimalField(default=0, verbose_name='\u0421\u0443\u043c\u043c\u0430 \u0441\u043f\u0438\u0441\u0430\u043d\u0438\u044f/\u043f\u043e\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u044f', max_digits=28, decimal_places=2)),
                ('note', models.TextField(default=b'', verbose_name='\u041f\u0440\u0438\u043c\u0435\u0447\u0430\u043d\u0438\u0435', blank=True)),
                ('character', models.ForeignKey(to='eveonline.EveCharacter')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
