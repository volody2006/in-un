# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext as _
from eveonline.models import EveCharacter

__author__ = 'user'

THE_PERIOD  =  (
    (1, _('year')),
    (2, _('month')),
    (3, _('week')),
    (4, _('day')),
    (5, _('hour')),
)

THE_TYPE  =  (
    (1, _('character')),
    (2, _('corporation')),
    (3, _('alliance')),
    (4, _('user')),
)

class Tariffs(models.Model):
    name = models.CharField(max_length=254, unique=True, verbose_name= _(u'Имя тарифа'))
    static_amount = models.DecimalField(max_digits=28, decimal_places=2, default=0, verbose_name = _(u'Статическая сумма'))
    percentage = models.DecimalField(max_digits=28, decimal_places=4, default=0, verbose_name = _(u'проценты'))
    limit_bounty = models.DecimalField(max_digits=28, decimal_places=2, default=0, verbose_name = _(u'Сумма заработка включенного в статическую сумму'))
    period = models.IntegerField(choices = THE_PERIOD, verbose_name = _(u'За какой период берется сумма'))
    type = models.IntegerField(choices = THE_TYPE, verbose_name = _(u'Тип тарифа (пилот, корпорация, альянс)'))
    date_created = models.DateTimeField(auto_now_add=True, verbose_name= _(u'Дата создания'))
    date_start = models.DateTimeField(blank=True, null=True, verbose_name= _(u'Дата старта тарифа'))
    date_stop = models.DateTimeField(blank=True, null=True, verbose_name= _(u'Дата окончания тарифа'))
    is_active = models.BooleanField(default=False, verbose_name= _(u'Тариф активен?'))
    note = models.TextField(verbose_name= _(u'Примечание'), blank=True, default='')
    user = models.ForeignKey(User)

    def __unicode__(self):
        return self.name


class Transaction(models.Model):
    user = models.ForeignKey(User)
    character = models.ForeignKey(EveCharacter)
    amount = models.DecimalField(max_digits=28, decimal_places=2, default=0, verbose_name= u'Сумма списания/пополнения')
  #  signed_amount = models.IntegerField(verbose_name = u'Сумма со знаком операции')
#    op_type = models.IntegerField(choices = OPERATION_TYPE_CHOICES, verbose_name = u'Тип операции')
    note = models.TextField(verbose_name= u'Примечание', blank=True, default='')


# Кому подключили тариф
class TariffConnected(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    character = models.ForeignKey(EveCharacter, blank=True, null=True, on_delete=models.SET_NULL)
    tariff = models.ForeignKey(Tariffs)
    date_created = models.DateTimeField(auto_now_add=True, verbose_name= _(u'Дата создания'))
    date_start = models.DateTimeField(blank=True, null=True, verbose_name= _(u'Дата старта тарифа'))
    date_stop = models.DateTimeField(blank=True, null=True, verbose_name= _(u'Дата окончания тарифа'))




