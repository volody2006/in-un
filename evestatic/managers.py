# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from eveonline.models import EveApiKeyPair, EveCharacter, EveCorporationInfo
from evestatic.models import MapRegions, MapConstellations, MapSolarSystems

from wallet.models import CharacterJournal, EntryType, CharacterTransactions, CorpJournal

__author__ = 'user'

def corp_wallet_journal(corporation_id):
    result = []
    try:
        corp_old = EveCorporationInfo.objects.using('c1inun').get(corporation_id=corporation_id)
        all_jornal = CorpJournal.objects.using('c1inun').filter(corporation=corp_old.id)

        for i in all_jornal:
            entry = {
                'result_id' :int(i.result_id),
                'result_date' : i.result_date,
                'type_id' : int(i.type_id.refTypeID),
                'party_1_name' : i.party_1_name,
                'party_1_id' : i.party_1_id,
                'party_1_type' : i.party_1_type,
                'party_2_name' : i.party_2_name,
                'party_2_id' : i.party_2_id,
                'party_2_type' : i.party_2_type,
                'arg_id' : i.arg_id,
                'arg_name' : i.arg_name,
                'result_amount' : i.result_amount,
                'result_balance' : i.result_balance,
                'result_reason' : i.result_reason,
                'tax_taxer_id'  : i.tax_taxer_id,
                'tax_amount' : i.tax_amount,
                'account' : i.account,
                'corporation' : corporation_id}
     #       print entry
            result.append(entry)

        result.sort(key=lambda x: x['result_id'])
        return result
    except:
        return result




def corp_jornal():
    all_corp = EveCorporationInfo.objects.all()
    # all_jornal = CharacterJournal.objects.using('c1inun').all()
    for corp in all_corp:
        print corp
        for i in corp_wallet_journal(corp.corporation_id):   #wall['party_1']
            print i
            obj, created = CorpJournal.objects.get_or_create(
                result_id = i['result_id'],
                defaults=dict(
                    result_date = i['result_date'],
                    type_id = EntryType.objects.get(refTypeID = i['type_id']),
                    party_1_name = i['party_1_name'],
                    party_1_id = i['party_1_id'],
                    party_1_type = i['party_1_type'],
                    party_2_name = i['party_2_name'],
                    party_2_id = i['party_2_id'],
                    party_2_type = i['party_2_type'],
                    arg_id = i['arg_id'],
                    arg_name = i['arg_name'],
                    result_amount = i['result_amount'],
                    result_balance = i['result_balance'],
                    result_reason = i['result_reason'],
                    tax_taxer_id = i['tax_taxer_id'],
                    tax_amount = i['tax_amount'],
                    account = int(i['account']),
                    corporation = corp
                )
            )


def t_wallet_journal(char_id):
    char_old = EveCharacter.objects.using('c1inun').get(character_id=char_id)
    all_jornal = CharacterTransactions.objects.using('c1inun').filter(character=char_old.id)
    result = []
    for i in all_jornal:
        entry = {
            'transactionID' :int(i.transactionID),
            'transactionDateTime' : i.transactionDateTime,
            'journalTransactionID' : int(i.journalTransactionID),
            'quantity' : i.quantity,
            'typeID' : i.typeID,
            'typeName' : i.typeName,
            'price' : i.price,
            'clientID' : i.clientID,
            'clientName' : i.clientName,
            'stationID' : i.stationID,
            'stationName' : i.stationName,
            'transactionType' : i.transactionType,
            'transactionFor' : i.transactionFor,
            'user' : i.user.username,
            'character' : int(char_id)}
 #       print entry
        result.append(entry)

    result.sort(key=lambda x: x['transactionID'])
    return result



def t_jornal_import():
    all_char = EveCharacter.objects.all()
   # all_jornal = CharacterJournal.objects.using('c1inun').all()
    for char in all_char:
        char_old = EveCharacter.objects.using('c1inun').get(character_id=char.character_id)
        all_jornal = CharacterTransactions.objects.using('c1inun').filter(character=char_old.id)
        print char_old
        #p_wallet_journal(char.character_id)
        for i in t_wallet_journal(char.character_id):   #wall['party_1']
            obj, created = CharacterTransactions.objects.get_or_create(
                transactionID = i['transactionID'],
                defaults=dict(
                    transactionDateTime = i['transactionDateTime'],
                    journalTransactionID = i['journalTransactionID'],
                    quantity = i['quantity'],
                    typeID = i['typeID'],
                    typeName = i['typeName'],
                    price = i['price'],
                    clientID = i['clientID'],
                    clientName = i['clientName'],
                    stationID = i['stationID'],
                    stationName = i['stationName'],
                    transactionType = i['transactionType'],
                    transactionFor = i['transactionFor'],
                    user = User.objects.get(username = i['user']),
                    character = char
                )
            )
            #print i



def all_api_import():
    all_api = EveApiKeyPair.objects.using('c1inun').all()
    for i in all_api:

        obj, created = EveApiKeyPair.objects.get_or_create(
            api_id =i.api_id,
            defaults=dict(
                api_key = i.api_key,
                user = i.user,

            ))
        print i.api_id

def char_import():
    all_char = EveCharacter.objects.using('c1inun').all()
    for i in all_char:

        obj, created = EveCharacter.objects.get_or_create(
            character_id =i.character_id,
            defaults=dict(
                character_name = i.character_name,
                user = i.user,
                corporation_id = i.corporation_id,
                corporation_name = i.corporation_name,
                corporation_ticker = i.corporation_ticker,
                alliance_id = i.alliance_id,
                alliance_name = i.alliance_name,
                api_id = i.api_id,

            ))
        print i.character_name

def p_wallet_journal(char_id):
    char_old = EveCharacter.objects.using('c1inun').get(character_id=char_id)
    all_jornal = CharacterJournal.objects.using('c1inun').filter(character=char_old.id)
    result = []
    for i in all_jornal:
        entry = {
            'result_id' :int(i.result_id),
            'result_date' : i.result_date,
            'type_id' : int(i.type_id.refTypeID),
            'party_1_name' : i.party_1_name,
            'party_1_id' : i.party_1_id,
            'party_1_type' : i.party_1_type,
            'party_2_name' : i.party_2_name,
            'party_2_id' : i.party_2_id,
            'party_2_type' : i.party_2_type,
            'arg_id' : i.arg_id,
            'arg_name' : i.arg_name,
            'result_amount' : i.result_amount,
            'result_balance' : i.result_balance,
            'result_reason' : i.result_reason,
            'tax_taxer_id'  : i.tax_taxer_id,
            'tax_amount' : i.tax_amount,
            'user' : i.user.username,
            'character' : int(char_id)}
 #       print entry
        result.append(entry)

    result.sort(key=lambda x: x['result_id'])
    return result



def jornal_import():
    all_char = EveCharacter.objects.all()
   # all_jornal = CharacterJournal.objects.using('c1inun').all()
    for char in all_char:
        char_old = EveCharacter.objects.using('c1inun').get(character_id=char.character_id)
        all_jornal = CharacterJournal.objects.using('c1inun').filter(character=char_old.id)
        print char_old
        #p_wallet_journal(char.character_id)
        if char.user == None:
            print 'Not user'
        for i in p_wallet_journal(char.character_id):   #wall['party_1']

            obj, created = CharacterJournal.objects.get_or_create(
                result_id = i['result_id'],
                defaults=dict(
                    result_date = i['result_date'],
                    type_id = EntryType.objects.get(refTypeID = i['type_id']),
                    party_1_name = i['party_1_name'],
                    party_1_id = i['party_1_id'],
                    party_1_type = i['party_1_type'],
                    party_2_name = i['party_2_name'],
                    party_2_id = i['party_2_id'],
                    party_2_type = i['party_2_type'],
                    arg_id = i['arg_id'],
                    arg_name = i['arg_name'],
                    result_amount = i['result_amount'],
                    result_balance = i['result_balance'],
                    result_reason = i['result_reason'],
                    tax_taxer_id = i['tax_taxer_id'],
                    tax_amount = i['tax_amount'],
                    user = User.objects.get(username = i['user']),
                    character = char
                )
            )
          #  print i


def mapregion_import():
    r = MapRegions.objects.using('eve').all()
    for i in r:

        obj, created = MapRegions.objects.get_or_create(
            region_id=i.region_id,
            defaults=dict(
                region_name = i.region_name,
                x = i.x,
                y = i.y,
                z = i.z,
                x_min = i.x_min,
                x_max = i.x_max,
                y_min = i.y_min,
                y_max = i.y_max,
                z_min = i.z_min,
                z_max = i.z_max,
                faction_id = i.faction_id,
                radius = i.radius)
        )


def mapconstellations_import():
    con = MapConstellations.objects.using('eve').all()

    for i in con:
        obj, created = MapConstellations.objects.get_or_create(
            constellation_id = i.constellation_id,
            defaults=dict(
                region_id = i.region_id,
                constellation_name = i.constellation_name,
                x = i.x,
                y = i.y,
                z = i.z,
                x_min = i.x_min,
                x_max = i.x_max,
                y_min = i.y_min,
                y_max = i.y_max,
                z_min = i.z_min,
                z_max = i.z_max,
                faction_id = i.faction_id,
                radius = i.radius)
        )


def mapsolarsystems_import():
    sys = MapSolarSystems.objects.using('eve').all()

    for i in sys:
        obj, created = MapSolarSystems.objects.get_or_create(
            solar_system_id= i.solar_system_id,
            defaults=dict(
                constellation_id = i.constellation_id,
                region_id = i.region_id,
                solar_system_name = i.solar_system_name,
                x = i.x,
                y = i.y,
                z = i.z,
                x_min = i.x_min,
                x_max = i.x_max,
                y_min = i.y_min,
                y_max = i.y_max,
                z_min = i.z_min,
                z_max = i.z_max,
                luminosity = i.luminosity,
                border = i.border,
                fringe = i.fringe,
                corridor = i.corridor,
                hub = i.hub,
                international = i.international,
                regional = i.regional,
                constellation = i.constellation,
                security = i.security,
                faction_id = i.faction_id,
                radius = i.radius,
                sun_type_id = i.sun_type_id,
                security_class = i.security_class,



            )
        )


