# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MapConstellations',
            fields=[
                ('constellation_id', models.IntegerField(serialize=False, primary_key=True, db_column='constellationID')),
                ('constellation_name', models.TextField(null=True, db_column='constellationName', blank=True)),
                ('x', models.TextField(null=True, blank=True)),
                ('y', models.TextField(null=True, blank=True)),
                ('z', models.TextField(null=True, blank=True)),
                ('x_min', models.TextField(null=True, db_column='xMin', blank=True)),
                ('x_max', models.TextField(null=True, db_column='xMax', blank=True)),
                ('y_min', models.TextField(null=True, db_column='yMin', blank=True)),
                ('y_max', models.TextField(null=True, db_column='yMax', blank=True)),
                ('z_min', models.TextField(null=True, db_column='zMin', blank=True)),
                ('z_max', models.TextField(null=True, db_column='zMax', blank=True)),
                ('faction_id', models.IntegerField(null=True, db_column='factionID', blank=True)),
                ('radius', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'mapConstellations',
            },
        ),
        migrations.CreateModel(
            name='MapRegions',
            fields=[
                ('region_id', models.IntegerField(serialize=False, primary_key=True, db_column='regionID')),
                ('region_name', models.TextField(null=True, db_column='regionName', blank=True)),
                ('x', models.TextField(null=True, blank=True)),
                ('y', models.TextField(null=True, blank=True)),
                ('z', models.TextField(null=True, blank=True)),
                ('x_min', models.TextField(null=True, db_column='xMin', blank=True)),
                ('x_max', models.TextField(null=True, db_column='xMax', blank=True)),
                ('y_min', models.TextField(null=True, db_column='yMin', blank=True)),
                ('y_max', models.TextField(null=True, db_column='yMax', blank=True)),
                ('z_min', models.TextField(null=True, db_column='zMin', blank=True)),
                ('z_max', models.TextField(null=True, db_column='zMax', blank=True)),
                ('faction_id', models.IntegerField(null=True, db_column='factionID', blank=True)),
                ('radius', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'mapRegions',
            },
        ),
        migrations.CreateModel(
            name='MapSolarSystems',
            fields=[
                ('solar_system_id', models.IntegerField(serialize=False, primary_key=True, db_column='solarSystemID')),
                ('solar_system_name', models.TextField(null=True, db_column='solarSystemName', blank=True)),
                ('x', models.TextField(null=True, blank=True)),
                ('y', models.TextField(null=True, blank=True)),
                ('z', models.TextField(null=True, blank=True)),
                ('x_min', models.TextField(null=True, db_column='xMin', blank=True)),
                ('x_max', models.TextField(null=True, db_column='xMax', blank=True)),
                ('y_min', models.TextField(null=True, db_column='yMin', blank=True)),
                ('y_max', models.TextField(null=True, db_column='yMax', blank=True)),
                ('z_min', models.TextField(null=True, db_column='zMin', blank=True)),
                ('z_max', models.TextField(null=True, db_column='zMax', blank=True)),
                ('luminosity', models.TextField(null=True, blank=True)),
                ('border', models.IntegerField(null=True, blank=True)),
                ('fringe', models.IntegerField(null=True, blank=True)),
                ('corridor', models.IntegerField(null=True, blank=True)),
                ('hub', models.IntegerField(null=True, blank=True)),
                ('international', models.IntegerField(null=True, blank=True)),
                ('regional', models.IntegerField(null=True, blank=True)),
                ('constellation', models.IntegerField(null=True, blank=True)),
                ('security', models.TextField(null=True, blank=True)),
                ('faction_id', models.IntegerField(null=True, db_column='factionID', blank=True)),
                ('radius', models.TextField(null=True, blank=True)),
                ('sun_type_id', models.IntegerField(null=True, db_column='sunTypeID', blank=True)),
                ('security_class', models.TextField(null=True, db_column='securityClass', blank=True)),
                ('constellation_id', models.ForeignKey(db_column='constellationID', blank=True, to='evestatic.MapConstellations', null=True)),
                ('region_id', models.ForeignKey(db_column='regionID', blank=True, to='evestatic.MapRegions', null=True)),
            ],
            options={
                'db_table': 'mapSolarSystems',
            },
        ),
        migrations.AddField(
            model_name='mapconstellations',
            name='region_id',
            field=models.ForeignKey(db_column='regionID', blank=True, to='evestatic.MapRegions', null=True),
        ),
    ]
