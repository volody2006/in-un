# -*- coding: utf-8 -*-
from alliance_auth import settings
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render_to_response
from django.template import RequestContext
from evemail.models import MailMessages
from django.http import request
from eveonline.models import EveCharacter


def evemail_all(request):
    render_items ={}
    list = request.GET.get('list')
    full_list = [25, 100, 500]
    if list is None: list = 50
    messages = MailMessages.objects.filter(user=request.user).order_by('-message_id')
    paginator = Paginator(messages, list)
    page = request.GET.get('page')
    try:
        messages = paginator.page(page)
    except PageNotAnInteger:
        messages = paginator.page(1)
    except EmptyPage:
        messages = paginator.page(paginator.num_pages)

    render_items= {
        'messages': messages,
    }



    return render_to_response('dashboard/evemail/mailbox.html', render_items, context_instance=RequestContext(request))

def evemail_read(request, character_id, id):
    message = MailMessages.objects.get(user=request.user,
                                       pk=id,
                                       character=EveCharacter.objects.get(character_id=character_id)
                                       )
    print message.message_id
    render_items= {
        'message': message,
    }
    return render_to_response('dashboard/evemail/read-mail.html', render_items, context_instance=RequestContext(request))
