# -*- coding: utf-8 -*-
from datetime import timedelta
from celery.task import periodic_task
from django.contrib.auth.models import User
from evemail.managers import load_mail_messages, load_mail_messages_headers, load_notifications, load_notification_texts
from eveonline.models import EveCharacter, EveApiKeyPair

@periodic_task(run_every = timedelta(minutes = 32))
def load_mail():
    chars = EveCharacter.objects.exclude(api_id=None)
    for char in chars:
        api = EveApiKeyPair.objects.get(api_id=char.api_id)
        message_ids = load_mail_messages_headers(char.character_id, api.api_id, api.api_key)
        if message_ids != False:
            load_mail_messages(char.character_id, api.api_id, api.api_key, message_ids)



@periodic_task(run_every = timedelta(minutes = 32))
def load_notification():
    chars = EveCharacter.objects.exclude(api_id=None)
   # chars = Character.objects.filter(character_id = 93959323)
    for char in chars:
        api = EveApiKeyPair.objects.get(api_id=char.api_id)
        notification_ids = load_notifications(char.character_id, api.api_id, api.api_key)
        if notification_ids != False:
            load_notification_texts(char.character_id, api.api_id, api.api_key, notification_ids)

a = {'alliance':
         {'timestamp': 1351882980, 'id': 99002594, 'name': 'RED University'},
     'sec_status': 5.00469942350697, 'skillpoints': None,
     'corp':
         {'timestamp': 1425535740, 'id': 1726357784, 'name': 'SOERI Academy'},
     'ship': {'type_name': None, 'name': None, 'type_id': None},
     'id': 2078467824, 'name': 'Android Strider', 'bloodline': 'Civire',
     'isk': None, 'race': 'Caldari', 'location': None,
     'history': [
         {'corp_name': 'SOERI Academy', 'start_ts': 1425535740, 'corp_id': 1726357784},
         {'corp_name': 'Caldari Provisions', 'start_ts': 1360007580, 'corp_id': 1000009},
         {'corp_name': 'SOERI Academy', 'start_ts': 1343076660, 'corp_id': 1726357784},
         {'corp_name': 'Caldari Provisions', 'start_ts': 1325779020, 'corp_id': 1000009},
         {'corp_name': 'SOERI Academy', 'start_ts': 1306080720, 'corp_id': 1726357784},
         {'corp_name': 'Caldari Provisions', 'start_ts': 1291305900, 'corp_id': 1000009},
         {'corp_name': 'Senators of Eridan', 'start_ts': 1265903160, 'corp_id': 1125657725},
         {'corp_name': 'SOERI Academy', 'start_ts': 1260912240, 'corp_id': 1726357784},
         {'corp_name': 'Dark GuaRDianS', 'start_ts': 1259750400, 'corp_id': 114913545},
         {'corp_name': 'Science and Trade Institute', 'start_ts': 1259463000, 'corp_id': 1000045}]}



