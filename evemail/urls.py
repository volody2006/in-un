# -*- coding: utf-8 -*-


from django.conf.urls import patterns, include, url
from evemail.views import *

urlpatterns = patterns('',

                       #url(r'^/', 'evemail.views.evemail_test', name='evemail_test'),
                       url(r'^/all/$', 'evemail.views.evemail_all', name='evemail_all'),
                       # url(r'^/all/$', 'evemail.views.evemail_all', name='evemail_all'),
                       url(r'^/read/(?P<character_id>\d+)/(?P<id>\d+)/$', 'evemail.views.evemail_read', name='evemail_read'),
                       # url('^write/$', 'evemail.views.evemail_write', name='evemail_write'),
                       # url('^write/(?P<recipient_id>\d+)/$', view='write', name='pm_write_user'),
                       # url('^system/$', view='system', name='pm_system'),
                       # url('^delete/(?P<message_id>\d+)/?$', view='delete',   name='pm_delete'),
                       #
                       # url(r'^wallet/journal/$', 'wallet.views.wallet_jornal_char_view', name='wallet_jo'),
                       #
                       # url(r'^wallet/journal/(?P<character_id>[^/]+)/$', 'wallet.views.wallet_jornal_char_view', name='wallet_jo'),
                       # url(r'^wallet/transactions/$', 'wallet.views.wallet_transaction_char_view', name='wallet_tr'),
                       # url(r'^wallet/transactions/(?P<character_id>[^/]+)/$', 'wallet.views.wallet_transaction_char_view', name='wallet_tr'),
                       # url(r'^wallet/statistic/$', 'wallet.views.stat_wallet_view', name='stat_wallet_view'),
                       # url(r'^wallet/statistic/(?P<character_id>[^/]+)/$', 'wallet.views.stat_wallet_view', name='stat_wallet_view'),
                       # url(r'^test/$', 'wallet.views.test', name='test'),
                       #
                       # Corp view
                       # url(r'^wallet/corp/journal/$', 'wallet.views.corp_wallet_jornal_view', name='corp_wallet_jornal_view'),
                       # url(r'^wallet/corp/journal/(?P<account>[^/]+)/$', 'wallet.views.corp_wallet_jornal_view', name='corp_wallet_jornal_view'),
                       # url(r'^wallet/corp/stat/$', 'wallet.views.corp_stat_bounty', name='corp_stat_bounty'),
                       # url(r'^wallet/corp/stat/(?P<corporation_id>[^/]+)/$', 'wallet.views.corp_stat_bounty', name='corp_stat_bounty'),
                       #
                       # url(r'^(?P<lang>[a-zA-Z]{2})vall/$', 'wallet.views.change_lang1', name='change_lang1'),




)