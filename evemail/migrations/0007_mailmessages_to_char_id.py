# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0007_auto_20150813_0437'),
        ('evemail', '0006_auto_20150819_1544'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailmessages',
            name='to_char_id',
            field=models.ManyToManyField(related_name='+', to='eveonline.EveCharacter'),
        ),
    ]
