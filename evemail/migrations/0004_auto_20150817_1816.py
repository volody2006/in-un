# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('evemail', '0003_auto_20150817_1656'),
    ]

    operations = [
        migrations.AddField(
            model_name='notifications',
            name='content',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='notifications',
            name='received',
            field=models.BooleanField(default=False),
        ),
    ]
