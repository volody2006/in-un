# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('evemail', '0005_mailmessages_is_read'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailmessages',
            name='is_delete',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='mailmessages',
            name='is_forward',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='mailmessages',
            name='is_reply',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='mailmessages',
            name='message_id',
            field=models.BigIntegerField(null=True, blank=True),
        ),
    ]
