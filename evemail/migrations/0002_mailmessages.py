# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('eveonline', '0007_auto_20150813_0437'),
        ('evemail', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MailMessages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message_id', models.BigIntegerField()),
                ('sender_id', models.IntegerField(null=True, blank=True)),
                ('sent_date', models.DateTimeField(null=True, blank=True)),
                ('title', models.TextField(null=True, blank=True)),
                ('to_org_id', models.CharField(max_length=254, null=True, blank=True)),
                ('to_char_ids', models.CharField(max_length=254, null=True, blank=True)),
                ('to_list_ids', models.CharField(max_length=254, null=True, blank=True)),
                ('content', models.TextField(default=b'')),
                ('mailbodies', models.BooleanField(default=False)),
                ('character', models.ForeignKey(to='eveonline.EveCharacter')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
    ]
