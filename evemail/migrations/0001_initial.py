# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0007_auto_20150813_0437'),
    ]

    operations = [
        migrations.CreateModel(
            name='MailingLists',
            fields=[
                ('list_id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=254)),
                ('character', models.ManyToManyField(to='eveonline.EveCharacter')),
            ],
        ),
    ]
