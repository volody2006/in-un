# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('eveonline', '0007_auto_20150813_0437'),
        ('evemail', '0002_mailmessages'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notifications',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('notification_id', models.IntegerField()),
                ('sender_id', models.IntegerField()),
                ('sent_date', models.DateTimeField(null=True, blank=True)),
                ('read', models.BooleanField(default=False)),
                ('character', models.ForeignKey(to='eveonline.EveCharacter')),
            ],
        ),
        migrations.CreateModel(
            name='NotificationTypes',
            fields=[
                ('type_id', models.IntegerField(serialize=False, primary_key=True)),
                ('description', models.CharField(max_length=254, null=True, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='notifications',
            name='type_id',
            field=models.ForeignKey(to='evemail.NotificationTypes'),
        ),
        migrations.AddField(
            model_name='notifications',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
