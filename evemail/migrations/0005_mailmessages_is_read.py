# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('evemail', '0004_auto_20150817_1816'),
    ]

    operations = [
        migrations.AddField(
            model_name='mailmessages',
            name='is_read',
            field=models.BooleanField(default=False),
        ),
    ]
