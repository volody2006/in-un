# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('eveonline', '0004_auto_20150808_1649'),
    ]

    operations = [
        migrations.CreateModel(
            name='AuthServicesInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ipboard_username', models.CharField(default=b'', max_length=254)),
                ('ipboard_password', models.CharField(default=b'', max_length=254)),
                ('forum_username', models.CharField(default=b'', max_length=254)),
                ('forum_password', models.CharField(default=b'', max_length=254)),
                ('jabber_username', models.CharField(default=b'', max_length=254)),
                ('jabber_password', models.CharField(default=b'', max_length=254)),
                ('mumble_username', models.CharField(default=b'', max_length=254)),
                ('mumble_password', models.CharField(default=b'', max_length=254)),
                ('teamspeak3_uid', models.CharField(default=b'', max_length=254)),
                ('teamspeak3_perm_key', models.CharField(default=b'', max_length=254)),
                ('is_blue', models.BooleanField(default=False)),
                ('main_char_id', models.ForeignKey(to='eveonline.EveCharacter')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
