# -*- coding: utf-8 -*-

from datetime import datetime
from alliance_auth import settings
from authentication.managers import AuthServicesInfoManager
from django.utils.timezone import make_aware
import evelink
from eveonline.models import EveCharacter
from eveonline.models import EveApiKeyPair
from eveonline.models import EveAllianceInfo
from eveonline.models import EveCorporationInfo
from services.managers.eve_api_manager import EveApiManager
from util import add_member_permission, check_if_user_has_permission, remove_member_permission
from util.common_task import add_user_to_group, generate_corp_group_name, remove_user_from_group, deactivate_services


class EveManager:
    def __init__(self):
        pass

    @staticmethod
    def check_access_bit(accessmask, bit):
        """ Returns a bool indicating if the bit is set in the accessmask """
        mask = 1 << bit
        return (accessmask & mask) > 0


    @staticmethod
    def update_or_create_character_base(character_id):
        """
        Создаем и обновляем заготовку пилота.
        :param character_id:
        :return: Возвращаем обьект если мы его создали(обновили) или Ложь, если произошла ошибка
        """

        eve = evelink.eve.EVE()
        try:
            # {'alliance': {'timestamp': 1429328880, 'id': 99005391, 'name': 'FEDERATION OF INDEPENDENT STELLAR SYSTEMS'}, 'sec_status': 5.00010879232042, 'skillpoints': None, 'corp': {'timestamp': 1440340140, 'id': 98366797, 'name': 'ATAC OTMOPO3OK B KOCMOCE.'}, 'ship': {'type_name': None, 'name': None, 'type_id': None}, 'id': 828748977, 'name': 'Firewine', 'bloodline': 'Achura', 'isk': None, 'race': 'Caldari', 'location': None, 'history': [{'corp_name': 'ATAC OTMOPO3OK B KOCMOCE.', 'start_ts': 1440340140, 'corp_id': 98366797}, {'corp_name': 'WARP Mechanics', 'start_ts': 1440084840, 'corp_id': 98337237}, {'corp_name': 'Eternal Flight.', 'start_ts': 1429952220, 'corp_id': 98386898}, {'corp_name': 'Eternal Monolith', 'start_ts': 1428950940, 'corp_id': 98342077}, {'corp_name': 'Perkone', 'start_ts': 1428327720, 'corp_id': 1000014}, {'corp_name': 'Eternal Monolith', 'start_ts': 1418070840, 'corp_id': 98342077}, {'corp_name': 'Perkone', 'start_ts': 1392173220, 'corp_id': 1000014}, {'corp_name': 'Brave Newbies Inc.', 'start_ts': 1391974320, 'corp_id': 98169165}, {'corp_name': 'Nanashi no Geemu', 'start_ts': 1391934300, 'corp_id': 98287908}, {'corp_name': 'Perkone', 'start_ts': 1266200220, 'corp_id': 1000014}, {'corp_name': 'The Perfect Storm', 'start_ts': 1264453140, 'corp_id': 591776049}, {'corp_name': 'Corporate Scum', 'start_ts': 1263537300, 'corp_id': 1818800266}, {'corp_name': 'Perkone', 'start_ts': 1262614800, 'corp_id': 1000014}, {'corp_name': 'Gulliver Corp', 'start_ts': 1260984600, 'corp_id': 1976829943}, {'corp_name': 'Perkone', 'start_ts': 1230480360, 'corp_id': 1000014}, {'corp_name': 'Gulliver Corp', 'start_ts': 1203169080, 'corp_id': 1976829943}, {'corp_name': 'Perkone', 'start_ts': 1202925240, 'corp_id': 1000014}, {'corp_name': 'Agony Unleashed', 'start_ts': 1182483240, 'corp_id': 793028819}, {'corp_name': 'Interstellar Operations Incorporated', 'start_ts': 1172629440, 'corp_id': 595925869}, {'corp_name': 'State War Academy', 'start_ts': 1172486040, 'corp_id': 1000167}]}

            character_info = eve.character_info_from_id(character_id).result
        except:
            print ('ERROR: update_or_create_character_base(character_id = %s )' % (character_id))
            return False
        obj, created = EveCharacter.objects.update_or_create(
            character_id = character_id,
            character_name = character_info['name'],
            defaults=dict(corporation_id = character_info['corp']['id'],
                          corporation_name = character_info['corp']['name'],
                          corporation_ticker = EveApiManager.get_corporation_ticker_from_id(character_info['corp']['id']),
                          alliance_id = character_info['alliance']['id'],
                          alliance_name = character_info['alliance']['name'],
                          )
        )

        return obj


    @staticmethod
    def update_or_create_corporation_base(corporation_id):
        """
        Создаем и обновляем заготовку корпорации.

        Если СЕО НПС персонаж ничего не делаем, возвращаем Ложь
        :param corporation_id:
        :return: Возвращаем обьект, если мы его создали(обновили) или Ложь, если произошла ошибка
        """
        corp_info = EveApiManager.get_corporation_information(corporation_id)
        ceo_id = corp_info['ceo']['id']
        # Если СЕО НПС Персонаж, ничего не делаем,  возвращаем Ложь.
        ceo = EveManager.update_or_create_character_base(ceo_id)
        if ceo is False:
            return False
        alliance = corp_info['alliance']['id']
        if alliance is not None:
            alliance = EveManager.update_or_create_alliance_info(alliance)
        description = corp_info['description'] #.decode('unicode-escape')
        obj, created = EveCorporationInfo.objects.update_or_create(
            corporation_id=corp_info['id'],
            corporation_name = corp_info['name'],
            corporation_ticker = corp_info['ticker'],
            defaults=dict(member_count = corp_info['members']['current'],
                          tax = corp_info['tax_percent'],
                          description = description,
                          ceo = ceo,
                          url = corp_info['url'],
                          shares = corp_info['shares'],
                          alliance = alliance,
                         # is_blue = is_blue
                         )
        )
        return obj


    @staticmethod
    def update_or_create_alliance_info(alliance_id):
        """
        Создаем или обновляем альянс
        :param alliance_id:
        :return:
        """
        try:
            alliance_info = EveApiManager.get_alliance_information(alliance_id)
            obj, created = EveAllianceInfo.objects.update_or_create(
                alliance_id=alliance_id,
                alliance_name = alliance_info['name'],
                alliance_ticker = alliance_info['ticker'],
                defaults=dict(executor_corp_id = int(alliance_info['executor_id']),
                              # is_blue = False,
                              member_count = alliance_info['member_count'],
                              is_active = True,

                              )
            )
            return obj
        except:
            print ('ERROR: Alliance could not be found.')
            return None






    @staticmethod
    def update_or_create_corporation_info(corporation_id):
        corp_info = EveApiManager.get_corporation_information(corporation_id)
        alliance_id = corp_info['alliance']['id']
        if alliance_id is not None:
            if EveManager.check_if_alliance_exists_by_id(alliance_id):
                alliance = EveAllianceInfo.objects.get(alliance_id=alliance_id)
            else:
                alliance = EveManager.update_or_create_alliance_info(alliance_id)[0]
        else:
            alliance = None
     #   print corp_info
        ceo_id = corp_info['ceo']['id']
        if not EveManager.check_if_character_id_exist(ceo_id):
            # Если СЕО НПС Персонаж, ничего не делаем,  возвращаем Ложь.
            if EveManager.character_info(ceo_id) is False:
                return False
        description = corp_info['description'] #.decode('unicode-escape')
        obj, created = EveCorporationInfo.objects.update_or_create(
            corporation_id=corp_info['id'],
            corporation_name = corp_info['name'],
            corporation_ticker = corp_info['ticker'],
            defaults=dict(member_count = corp_info['members']['current'],
                          alliance = alliance,
                          tax = corp_info['tax_percent'],
                          description = description,
                          ceo = EveCharacter.objects.get(character_id=ceo_id),
                          url = corp_info['url'],
                          shares = corp_info['shares'],
                         # is_blue = is_blue
                         )
        )
        return obj, created


    @staticmethod
    def char_or_corp_or_alliance_id(id):
        pass


    @staticmethod
    def character_info(character_id):
        eve = evelink.eve.EVE()
        try:
            character_info = eve.character_info_from_id(character_id).result
        except:
            return False
        name = character_info['name']
        if name.endswith(str(character_id)):
            print 'None char: %s' % name
            return False
        if character_info['alliance']['id'] == None:
            alliance = None
        else:
            alliance = EveManager.get_alliance_info_by_id(character_info['alliance']['id'])

            if not EveManager.check_if_alliance_exists_by_id(character_info['alliance']['id']):
                alliance_info = EveApiManager.get_alliance_information(character_info['alliance']['id'])
                print u'Создаем альянс'
                EveManager.create_alliance_info(character_info['alliance']['id'],
                                          alliance_info['name'],
                                          alliance_info['ticker'],
                                          alliance_info['executor_id'],
                                          alliance_info['member_count'],
                                          False)

        if not EveManager.check_if_corporation_exists_by_id(character_info['corp']['id']):
            corpinfo = EveApiManager.get_corporation_information(character_info['corp']['id'])
            print u'Создаем корпу'
            EveManager.create_corporation_info(corpinfo['id'], corpinfo['name'], corpinfo['ticker'],
                                         corpinfo['members']['current'], False, alliance)

        if not EveManager.check_if_character_exist(character_info['name']):
            EveManager.create_character(character_id = character_id,
                                            character_name = character_info['name'],
                                            corporation_id = character_info['corp']['id'],
                                            corporation_name = character_info['corp']['name'],
                                            corporation_ticker = EveCorporationInfo.objects.get(corporation_id=character_info['corp']['id']).corporation_ticker,
                                            alliance_id = character_info['alliance']['id'],
                                            alliance_name= character_info['alliance']['name'],
                                            user = None,
                                            api_id = None
                                        )
        return character_info


    @staticmethod
    def create_character(character_id, character_name, corporation_id,
                         corporation_name, corporation_ticker, alliance_id,
                         alliance_name, user, api_id):
        obj, created = EveCharacter.objects.update_or_create(
            character_id = character_id,
            character_name = character_name,
            defaults=dict(corporation_id = corporation_id,
                          corporation_name = corporation_name,
                          corporation_ticker = corporation_ticker,
                          alliance_id = alliance_id,
                          alliance_name = alliance_name,
                          user = user,
                          api_id= api_id))


    @staticmethod
    def create_characters_from_list(chars, user, api_id):

        for char in chars.result:
            #  if not EveManager.check_if_character_exist(chars.result[char]['name']):
            EveManager.create_character(chars.result[char]['id'],
                                        chars.result[char]['name'],
                                        chars.result[char]['corp']['id'],
                                        chars.result[char]['corp']['name'],
                                        EveApiManager.get_corporation_ticker_from_id(
                                            chars.result[char]['corp']['id']),
                                        chars.result[char]['alliance']['id'],
                                        chars.result[char]['alliance']['name'],
                                        user, api_id)

    @staticmethod
    def update_characters_from_list(chars):
        for char in chars.result:
            if EveManager.check_if_character_exist(chars.result[char]['name']):
                eve_char = EveManager.get_character_by_character_name(chars.result[char]['name'])
                eve_char.corporation_id = chars.result[char]['corp']['id']
                eve_char.corporation_name = chars.result[char]['corp']['name']
                eve_char.corporation_ticker = EveApiManager.get_corporation_ticker_from_id(
                    chars.result[char]['corp']['id'])
                eve_char.alliance_id = chars.result[char]['alliance']['id']
                eve_char.alliance_name = chars.result[char]['alliance']['name']
                eve_char.save()


    @staticmethod
    def create_api_keypair(api_id, api_key, user):
        api_info = EveApiManager.get_api_info(api_id, api_key)
        if api_info.result['type'] == 'corp':
            is_corp = True
            corp_info = EveApiManager.get_corporation_information( api_id=api_id, api_key=api_key)
            corporation, created = EveManager.update_or_create_corporation_info(corp_info['id'])
        else:
            is_corp = False
            corporation = None
        mask = api_info.result['access_mask']
        expire = api_info.result['expire_ts']
        if expire != None:
            expire = make_aware(datetime.utcfromtimestamp(expire))
        obj, creade = EveApiKeyPair.objects.update_or_create(
                                            api_id = api_id,
                                            api_key = api_key,
                                            defaults=dict(
                                                        user = user,
                                                        active = True,
                                                        is_corp = is_corp,
                                                        mask = mask,
                                                        expire = expire,
                                                        corporation = corporation,
                                            )
        )
        # if not ApiKeyPair.objects.filter(api_id=api_id).exists():
        #     api_pair = ApiKeyPair()
        #     api_pair.api_id = api_id
        #     api_pair.api_key = api_key
        #     api_pair.user = user_id
        #     api_pair.active = True
        #     api_info = EveApiManager.get_api_info(api_id, api_key)
        #     if api_info.result['type'] == 'corp':
        #         api_pair.is_corp = True
        #     else:
        #         api_pair.is_corp = False
        #     api_pair.mask = api_info.result['access_mask']
        #     expire = api_info.result['expire_ts']
        #     if expire != None:
        #         api_pair.expire = datetime.utcfromtimestamp(expire)
        #     else:
        #         api_pair.expire = expire
        #     api_pair.save()
        # else:
        #     api_pair = ApiKeyPair.objects.get(api_id=api_id)
        #     api_pair.api_id = api_id
        #     api_pair.api_key = api_key
        #     api_pair.user = user_id
        #     api_pair.active = True
        #     api_info = EveApiManager.get_api_info(api_id, api_key)
        #     if api_info.result['type'] == 'corp':
        #         api_pair.is_corp = True
        #     else:
        #         api_pair.is_corp = False
        #     api_pair.mask = api_info.result['access_mask']
        #     expire = api_info.result['expire_ts']
        #     if expire != None:
        #         api_pair.expire = datetime.utcfromtimestamp(expire)
        #     else:
        #         api_pair.expire = expire
        #     api_pair.save()

    @staticmethod
    def create_alliance_info(alliance_id, alliance_name, alliance_ticker, alliance_executor_corp_id,
                             alliance_member_count, is_blue):
        if not EveManager.check_if_alliance_exists_by_id(alliance_id):
            alliance_info = EveAllianceInfo()
            alliance_info.alliance_id = alliance_id
            alliance_info.alliance_name = alliance_name
            alliance_info.alliance_ticker = alliance_ticker
            alliance_info.executor_corp_id = alliance_executor_corp_id
            alliance_info.member_count = alliance_member_count
            alliance_info.is_blue = is_blue
            alliance_info.save()

    @staticmethod
    def update_alliance_info(alliance_id, alliance_executor_corp_id, alliance_member_count, is_blue, is_active=True):
        if EveManager.check_if_alliance_exists_by_id(alliance_id):
            alliance_info = EveAllianceInfo.objects.get(alliance_id=alliance_id)
            alliance_info.executor_corp_id = alliance_executor_corp_id
            alliance_info.member_count = alliance_member_count
            alliance_info.is_blue = is_blue
            alliance_info.is_active = is_active
            alliance_info.save()

    @staticmethod
    def create_corporation_info(corp_id, corp_name, corp_ticker, corp_member_count, is_blue, alliance):
        if not EveManager.check_if_corporation_exists_by_id(corp_id):
            corp_info = EveCorporationInfo()
            corp_info.corporation_id = corp_id
            corp_info.corporation_name = corp_name
            corp_info.corporation_ticker = corp_ticker
            corp_info.member_count = corp_member_count
            corp_info.is_blue = is_blue
            if alliance:
                corp_info.alliance = alliance
            else:
                corp_info.alliance = None
            corp_info.save()

    @staticmethod
    def update_corporation_info(corp_id, corp_member_count, alliance, is_blue):
        if EveManager.check_if_corporation_exists_by_id(corp_id):
            corp_info = EveCorporationInfo.objects.get(corporation_id=corp_id)
            corp_info.member_count = corp_member_count
            corp_info.alliance = alliance
            corp_info.is_blue = is_blue
            corp_info.save()


    @staticmethod
    def update_or_create_character_info(character_id):
        pass

    @staticmethod
    def get_api_key_pairs(user):
        if EveApiKeyPair.objects.filter(user=user).exists():
            return EveApiKeyPair.objects.filter(user=user)

    @staticmethod
    def check_if_api_key_pair_exist(api_id):
        if EveApiKeyPair.objects.filter(api_id=api_id).exists():
            return True
        else:
            return False

    @staticmethod
    # Удаляем у данного юзера апи. сам апи ключь оставляем.
    def delete_api_key_pair_user(api_id, user_id):
        if EveApiKeyPair.objects.filter(api_id=api_id).exists():
            # Check that its owned by our user_id
            apikeypair = EveApiKeyPair.objects.get(api_id=api_id)
            if apikeypair.user.id == user_id:

              # apikeypair.active= False
                apikeypair.user_id = None
                apikeypair.save()


    @staticmethod
    # Деактивируем апи ключ.
    def delete_api_key_pair(api_id):
        apikeypair = EveApiKeyPair.objects.get(api_id=api_id)
        apikeypair.delete()
        characters = EveCharacter.objects.filter(api_id=api_id)
        for char in characters:
            char.api_id = None
            char.user = None
            char.save()


    @staticmethod
    def update_api_key_pair(api_id, api_key):
        apikeypair = EveApiKeyPair.objects.get(api_id=api_id)
        api_info = EveApiManager.get_api_info(api_id, api_key)
        apikeypair.mask = api_info.result['access_mask']
        if api_info.result['type'] == 'corp':
            apikeypair.is_corp = True
        else:
            apikeypair.is_corp = False
        if api_info.result['expire_ts'] != None:
            apikeypair.expire = make_aware(datetime.utcfromtimestamp(api_info.result['expire_ts']))
        else:
            apikeypair.expire = api_info.result['expire_ts']
        apikeypair.save()


    @staticmethod
    def delete_characters_by_api_id(api_id, user_id):
        if EveCharacter.objects.filter(api_id=api_id).exists():
            # Check that its owned by our user_id
            characters = EveCharacter.objects.filter(api_id=api_id)

            for char in characters:
                if char.user.id == user_id:
                    char.api_id = None
                    char.user = None
                    char.save()
                  #  char.delete()


    @staticmethod
    def check_if_character_exist(char_name):
        return EveCharacter.objects.filter(character_name=char_name).exists()


    @staticmethod
    def check_if_character_id_exist(char_id):
        return EveCharacter.objects.filter(character_id=char_id).exists()

    @staticmethod
    def get_characters_by_owner_id(user):
        if EveCharacter.objects.filter(user=user).exists():
            return EveCharacter.objects.filter(user=user)

        return None

    @staticmethod
    def get_character_by_character_name(char_name):
        if EveCharacter.objects.filter(character_name=char_name).exists():
            return EveCharacter.objects.get(character_name=char_name)

    @staticmethod
    def get_character_by_id(char_id):
        if EveCharacter.objects.filter(character_id=char_id).exists():
            return EveCharacter.objects.get(character_id=char_id)
        return None

    @staticmethod
    def get_charater_alliance_id_by_id(char_id):
        if EveCharacter.objects.filter(character_id=char_id).exists():
            return EveCharacter.objects.get(character_id=char_id).alliance_id

    @staticmethod
    def check_if_character_owned_by_user(char_id, user):
        character = EveCharacter.objects.get(character_id=char_id)

        if character.user.id == user.id:
            return True

        return False

    @staticmethod
    def check_if_alliance_exists_by_id(alliance_id):
        return EveAllianceInfo.objects.filter(alliance_id=alliance_id).exists()

    @staticmethod
    def check_if_corporation_exists_by_id(corp_id):
        return EveCorporationInfo.objects.filter(corporation_id=corp_id).exists()

    @staticmethod
    def get_alliance_info_by_id(alliance_id):
        if EveManager.check_if_alliance_exists_by_id(alliance_id):
            return EveAllianceInfo.objects.get(alliance_id=alliance_id)
        else:
            return None

    @staticmethod
    def get_corporation_info_by_id(corp_id):
        if EveManager.check_if_corporation_exists_by_id(corp_id):
            return EveCorporationInfo.objects.get(corporation_id=corp_id)
        else:
            return None

    @staticmethod
    def get_all_corporation_info():
        return EveCorporationInfo.objects.all()

    @staticmethod
    def get_all_alliance_info():
        return EveAllianceInfo.objects.all()

    @staticmethod
    def get_all_char_corp(corp_id):
        return EveCharacter.objects.filter(corporation_id = corp_id)


    @staticmethod
    def update_or_create_corp_divisions (api_id, api_key):
        pass


    @staticmethod
    def main_character(user, char_id):

        if EveManager.check_if_character_owned_by_user(char_id, user):
            previousmainid = AuthServicesInfoManager.get_auth_service_info(user).main_char_id
            AuthServicesInfoManager.update_main_char_Id(char_id, user)
            # Check if character is in the alliance
            character_info = EveManager.get_character_by_id(char_id)
            corporation_info = EveManager.get_corporation_info_by_id(character_info.corporation_id)

            if EveManager.get_charater_alliance_id_by_id(char_id) == settings.ALLIANCE_ID:
                add_member_permission(user, 'alliance_member')
                add_user_to_group(user, settings.DEFAULT_ALLIANCE_GROUP)
                add_user_to_group(user,
                                  generate_corp_group_name(EveManager.get_character_by_id(char_id).corporation_name))

            elif corporation_info != None:
                if corporation_info.is_blue:
                    add_member_permission(user, 'blue_member')
                    add_user_to_group(user, settings.DEFAULT_BLUE_GROUP)
                    AuthServicesInfoManager.update_is_blue(True, user)
                else:
                    if check_if_user_has_permission(user, 'alliance_member'):
                        disable_alliance_member(user, previousmainid)

                    if check_if_user_has_permission(user, 'blue_member'):
                        disable_blue_member(user)
            else:
                # TODO: disable serivces
                if check_if_user_has_permission(user, 'alliance_member'):
                    disable_alliance_member(user, previousmainid)

                if check_if_user_has_permission(user, 'blue_member'):
                    disable_blue_member(user)

def disable_alliance_member(user, char_id):
    remove_member_permission(user, 'alliance_member')
    remove_user_from_group(user, settings.DEFAULT_ALLIANCE_GROUP)
    remove_user_from_group(user,
                           generate_corp_group_name(
                               EveManager.get_character_by_id(char_id).corporation_name))
    deactivate_services(user)


def disable_blue_member(user):
    remove_member_permission(user, 'blue_member')
    remove_user_from_group(user, settings.DEFAULT_BLUE_GROUP)
    deactivate_services(user)