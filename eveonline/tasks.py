# -*- coding: utf-8 -*-
from datetime import datetime, time, timedelta
from time import sleep

from django.conf import settings
from celery.task import periodic_task
from celery.schedules import crontab
from django.contrib.auth.models import User
from eveonline.managers import EveManager
from eveonline.models import EveCharacter, EveApiKeyPair, EveCorporationInfo
from services.managers.eve_api_manager import EveApiManager


#Обновляем альянс
from util.eve_who import EVEWho


@periodic_task(run_every=crontab(minute=0, hour="*/2"))
def self_alliance_corp_update():
    #
    if EveApiManager.check_if_api_server_online():
        alliances_info = EveApiManager.get_all_alliance_information()
        alliance_info = alliances_info[int(settings.ALLIANCE_ID)]
        # Populate alliance info
        if not EveManager.check_if_alliance_exists_by_id(settings.ALLIANCE_ID):
            EveManager.update_or_create_alliance_info(settings.ALLIANCE_ID)

        alliance = EveManager.get_alliance_info_by_id(settings.ALLIANCE_ID)

        # Create the corps in the alliance
        for corporation_id in alliance_info['member_corps']:
            print EveManager.update_or_create_corporation_base(corporation_id)


@periodic_task(run_every = timedelta(hours = 23, minutes = 20))
def char_info_update():
    if EveApiManager.check_if_api_server_online():
        all_char = EveCharacter.objects.all()
        for char in all_char:
            # Обновляем базовую инфу по чару
            EveManager.update_or_create_character_base(char.character_id)
          #  print char.character_name
            # Если у чара есть привязка к апи, ищем его в базе, если апи удалили, то отменяем привязку.
            if char.api_id != None:

                try:
                    if not EveApiKeyPair.objects.filter(api_id=char.api_id).exists():
                        EveManager.delete_characters_by_api_id(char.api_id, char.user.id)
                        print ('Delete api in char: %s' % char.character_name)

                except:

                    print ('Delete api in char: %s' % char.character_name)
                    char.api_id = None
                    char.user = None
                    char.save()

# Run every 3 hours
# @periodic_task(run_every=crontab(minute=0, hour="*/3"))
# def char_api_update():
#     '''
#     В этой функции ищем ошибки и исправляем их
#     1. Находим чаров, у которых привязан апи, а самого ключа уже нет в базе.
#        У таких чаров удаляем юзера и апи.
#     :return:
#     '''
#     users = User.objects.all()
#     for user in users:
#         pass

def char_parse():
    if EveApiManager.check_if_api_server_online():
        all_char = EveCharacter.objects.all()
        from django.db.models import Max, Min
        max_id = all_char.aggregate(Max('character_id'))['character_id__max']
        min_id = 91044621 #91186422
      #  min_id = 90056248
        print max_id, min_id
        while min_id < max_id:
            min_id = min_id + 1

            if not EveCharacter.objects.filter(character_id=min_id).exists():
                char = EveManager.character_info(min_id)
                if char:
                    print char['name']
                else:
                    print False
                #sleep(0.5)
            else:
                print u'Персонаж %s есть в базе, пропускаем...' % min_id

# http://evewho.com/api.php?type=allilist&id=99001648
# http://evewho.com/api.php?type=corplist&id=869043665
# Парсим evewho
#>>> from eveonline.tasks import *
#>>> parse_evewho()

#
def parse_evewho():
    evewho = EVEWho()
    corps = EveCorporationInfo.objects.all()
    for corp in corps:
        print corp.corporation_id, corp.corporation_name
        list = evewho.corp_member_list(corp.corporation_id)
        #print list
        for char in list:
            if not EveCharacter.objects.filter(character_id=char['char_id']).exists():
                char_info = EveManager.character_info(char['char_id'])
                if char_info:
                    print char_info['name']
                else:
                    print False
                #sleep(0.5)
            else:
                print u'Персонаж %s есть в базе, пропускаем...' % char['char_id']
           # print char['char_id']


