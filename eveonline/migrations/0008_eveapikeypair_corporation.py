# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0007_auto_20150813_0437'),
    ]

    operations = [
        migrations.AddField(
            model_name='eveapikeypair',
            name='corporation',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.EveCorporationInfo', null=True),
        ),
    ]
