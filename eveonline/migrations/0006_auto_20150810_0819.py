# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0005_auto_20150810_0812'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evecorporationinfo',
            name='url',
            field=models.URLField(max_length=254, null=True, blank=True),
        ),
    ]
