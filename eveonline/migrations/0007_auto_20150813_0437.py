# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0006_auto_20150810_0819'),
    ]

    operations = [
        migrations.AddField(
            model_name='eveallianceinfo',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 13, 4, 36, 39, 491450, tzinfo=utc), verbose_name=b'Created Date/Time', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='eveallianceinfo',
            name='update',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 13, 4, 36, 44, 926270, tzinfo=utc), verbose_name=b'Last Update Date/Time', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='evecharacter',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 13, 4, 37, 1, 220492, tzinfo=utc), verbose_name=b'Created Date/Time', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='evecharacter',
            name='update',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 13, 4, 37, 5, 981390, tzinfo=utc), verbose_name=b'Last Update Date/Time', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='evecorporationinfo',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 13, 4, 37, 11, 82566, tzinfo=utc), verbose_name=b'Created Date/Time', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='evecorporationinfo',
            name='update',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 13, 4, 37, 15, 731365, tzinfo=utc), verbose_name=b'Last Update Date/Time', auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='evemapsovereignty',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 13, 4, 37, 19, 871609, tzinfo=utc), verbose_name=b'Created Date/Time', auto_now_add=True),
            preserve_default=False,
        ),
    ]
