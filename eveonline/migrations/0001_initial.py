# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings



class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Alliance',
            fields=[
                ('alliance_id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('alliance_name', models.CharField(unique=True, max_length=254)),
                ('alliance_ticker', models.CharField(max_length=254)),
                ('executor_corp', models.CharField(max_length=254)),
                ('is_blue', models.BooleanField(default=False)),
                ('member_count', models.IntegerField()),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='ApiKeyPair',
            fields=[
                ('api_id', models.IntegerField(serialize=False, primary_key=True)),
                ('api_key', models.CharField(unique=True, max_length=254)),
                ('mask', models.BigIntegerField(default=0, verbose_name=b'Access Mask')),
                ('active', models.BooleanField(default=True, verbose_name=b'Active')),
                ('is_corp', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('expire', models.DateTimeField(default=None, null=True, blank=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Character',
            fields=[
                ('character_id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('character_name', models.CharField(unique=True, max_length=254)),
                ('corporation_id', models.CharField(max_length=254)),
                ('corporation_name', models.CharField(max_length=254)),
                ('corporation_ticker', models.CharField(max_length=254)),
                ('alliance_id', models.CharField(max_length=254, null=True, blank=True)),
                ('alliance_name', models.CharField(max_length=254, null=True, blank=True)),
                ('api_id', models.CharField(max_length=254, null=True, blank=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Corporation',
            fields=[
                ('corporation_id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('corporation_name', models.CharField(unique=True, max_length=254)),
                ('corporation_ticker', models.CharField(max_length=254)),
                ('member_count', models.IntegerField()),
                ('is_blue', models.BooleanField(default=False)),
                ('tax', models.DecimalField(default=0, max_digits=28, decimal_places=2)),
                ('alliance', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.EveAllianceInfo', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='MapSovereignty',
            fields=[
                ('system_id', models.IntegerField(serialize=False, primary_key=True)),
                ('system_name', models.CharField(unique=True, max_length=254)),
                ('faction_id', models.IntegerField(null=True, blank=True)),
                ('alliance_id', models.IntegerField(null=True, blank=True)),
                ('corp_id', models.IntegerField(null=True, blank=True)),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
            ],
        ),
    ]
