# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0004_auto_20150808_1649'),
    ]

    operations = [
        migrations.AddField(
            model_name='evecorporationinfo',
            name='ceo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='eveonline.EveCharacter', null=True),
        ),
        migrations.AddField(
            model_name='evecorporationinfo',
            name='description',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='evecorporationinfo',
            name='shares',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='evecorporationinfo',
            name='url',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='evecorporationinfo',
            name='member_count',
            field=models.IntegerField(default=0),
        ),
    ]
