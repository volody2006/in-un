# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations



class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0003_evecorporationinfo_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='evecharacter',
            name='id',
        ),
        migrations.RemoveField(
            model_name='evecorporationinfo',
            name='id',
        ),
    ]
