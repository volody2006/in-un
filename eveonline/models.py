# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.utils.datetime_safe import datetime


class EveCharacter(models.Model):
    character_id = models.BigIntegerField(primary_key=True)
    character_name = models.CharField(max_length=254, unique=True)
    corporation_id = models.CharField(max_length=254)
    corporation_name = models.CharField(max_length=254)
    corporation_ticker = models.CharField(max_length=254)
    alliance_id = models.CharField(max_length=254, blank=True, null=True)
    alliance_name = models.CharField(max_length=254, blank=True, null=True)
    api_id = models.CharField(max_length=254, blank=True, null=True)
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)
 #   tariff = models.ForeignKey(Tariffs, blank=True, null=True, on_delete=models.SET_NULL)
 #   id = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return self.character_name





class EveAllianceInfo(models.Model):
    alliance_id = models.BigIntegerField(primary_key=True)
    alliance_name = models.CharField(max_length=254, unique=True)
    alliance_ticker = models.CharField(max_length=254)
    executor_corp_id = models.CharField(max_length=254)
    is_blue = models.BooleanField(default=False)
    member_count = models.IntegerField()
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)
 #   tariff = models.ForeignKey(Tariffs, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.alliance_name


class EveCorporationInfo(models.Model):
    corporation_id = models.BigIntegerField(primary_key=True)
    corporation_name = models.CharField(max_length=254, unique=True)
    corporation_ticker = models.CharField(max_length=254)
    member_count = models.IntegerField(default=0)
    is_blue = models.BooleanField(default=False)
    alliance = models.ForeignKey(EveAllianceInfo, blank=True, null=True, on_delete=models.SET_NULL)
    tax = models.DecimalField(max_digits=28, decimal_places=2, default=0)
    description = models.TextField(blank=True, null=True)
    ceo = models.ForeignKey(EveCharacter, blank=True, null=True, on_delete=models.SET_NULL)
    url = models.URLField(max_length = 254, blank=True, null=True)
    shares = models.IntegerField(default=0)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)
#    tariff = models.ForeignKey(Tariffs, blank=True, null=True, on_delete=models.SET_NULL)
#    id = models.IntegerField(blank=True, null=True)


    def __str__(self):
        return self.corporation_name

class EveMapSovereignty(models.Model):
    system_id = models.IntegerField(primary_key=True)
    system_name = models.CharField(max_length=254, unique=True)
    faction_id = models.IntegerField(blank=True, null=True)
    alliance_id = models.IntegerField(blank=True, null=True)
    corp_id = models.IntegerField(blank=True, null=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)


    def __str__(self):
        return self.system_id

class Divisions(models.Model):
    account_key = models.IntegerField()
    description = models.CharField(max_length=254)

    class Meta(object):
        abstract = True


class EveApiKeyPair(models.Model):
    api_id = models.IntegerField(primary_key=True)
    api_key = models.CharField(max_length=254, unique=True)
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    mask = models.BigIntegerField('Access Mask', default=0)
    active = models.BooleanField('Active', default=True)
    is_corp = models.BooleanField(default=False)
    created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    update = models.DateTimeField('Last Update Date/Time', auto_now=True)
    expire = models.DateTimeField(blank=True, null=True, default=None)
    corporation = models.ForeignKey(EveCorporationInfo, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        if self.user == None:
            return "None - ApiKeyPair"
        return self.user.username + " - ApiKeyPair"