from django.contrib import admin

from models import EveCharacter, EveMapSovereignty
from models import EveApiKeyPair
from models import EveAllianceInfo
from models import EveCorporationInfo

class EveApiKeyPairAdmin(admin.ModelAdmin):
    list_filter = ['user', 'is_corp', 'expire', 'active']
    search_fields = ['user__username', 'api_id']
    list_display = ('user','api_id', 'mask', 'active', 'is_corp')

class EveCharacterAdmin(admin.ModelAdmin):
    list_filter = ['alliance_name']
    search_fields = ['character_name', 'alliance_name', 'user__username',
                     'corporation_name', 'character_id', 'api_id', 'user__username']
    list_display = ('character_name','corporation_ticker', 'corporation_name', 'user', 'api_id')

class EveAllianceInfoAdmin(admin.ModelAdmin):
    list_filter = ['alliance_name', 'is_blue', 'is_active']
    search_fields = ['alliance_ticker', 'alliance_name', 'alliance_id', 'executor_corp']
    list_display = ('alliance_name','alliance_ticker', 'member_count', 'is_blue', 'is_active')

class EveCorporationInfoAdmin(admin.ModelAdmin):
    list_filter = ['tax','is_blue', 'alliance' ]
    search_fields = ['corporation_name', 'corporation_ticker', 'alliance__alliance_name', 'corporation_id']
    list_display = ('corporation_name','corporation_ticker', 'member_count', 'is_blue', 'tax', 'alliance')


class EveMapSovereigntyAdmin(admin.ModelAdmin):
    list_filter = [ ]
    search_fields = []
    list_display = ()



admin.site.register(EveCharacter, EveCharacterAdmin)
admin.site.register(EveApiKeyPair, EveApiKeyPairAdmin)
admin.site.register(EveAllianceInfo, EveAllianceInfoAdmin)
admin.site.register(EveCorporationInfo, EveCorporationInfoAdmin)
#admin.site.register(MapSovereignty, EveMapSovereigntyAdmin)