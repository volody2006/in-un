# -*- coding: utf-8 -*-
from alliance_auth import settings
import eveonline

__author__ = 'user'

from celery.task import periodic_task
from datetime import timedelta
from celery.schedules import crontab

from django.contrib.auth.models import User
from eveonline.models import EveCharacter, EveApiKeyPair, EveMapSovereignty, EveAllianceInfo, EveCorporationInfo
from eveonline.managers import EveManager, EveApiManager
from wallet.managers import *


# @periodic_task(run_every = timedelta(minutes = 1))
# def server_log():
#     try:
#         api = evelink.api.API()
#         server = evelink.server.Server(api=api)
#         info = server.server_status()
#         players = info.result['players']
#         online = info.result['online']
#         date_time = datetime.utcfromtimestamp(info.timestamp)
#         obj = EveServerStatus(players=players, status=online, datetime=date_time)
#         obj.save()
#     except evelink.api.APIError as error:
#         pass

@periodic_task(run_every = timedelta(days = 1))
def UserReport():
    list = [0, 1]
    for i in list:
        user_report_bounty(type_period=i)
    # users = User.objects.all()
    # for user in users:
    #     pass
    # try:
    #     type_period = 0
    #     to_date = month_period(period=type_period)[1]
    #     from_date = month_period(period=type_period)[0]
    # except:
    #     to_date = None
    #     from_date = None
    # corporation_id= 98292612
    # corp_list = EveManager.get_all_corporation_info()
    # alliance = EveManager.get_alliance_info_by_id(alliance_id = settings.ALLIANCE_ID)
    # corp_list = corp_list.filter(alliance = alliance)
    # result = []
    # chars = EveManager.get_all_char_corp(corp_id=corporation_id)
    # for char in chars:
    #     char_id = char.character_id
    #     summa = sum_bounty_character_from_corp(character_id = char_id, to_date=to_date, from_date=from_date)
    #     if summa is False:
    #         continue
    #     result.append([char, summa])
    # render_items = {'corps': corp_list,
    #                 'corp' : EveManager.get_corporation_info_by_id(corp_id=corporation_id),
    #                 'characters': chars,
    #                 'summa': result,
    #                 }
#

#python manage.py celeryd -l INFO -B
@periodic_task(run_every = timedelta(minutes = 31))
#@periodic_task(run_every=crontab(minute="*/31"))

def updating_characters_wallets():

    characters = EveCharacter.objects.exclude(api_id = None)
    for character in characters:
        print character, character.character_id
        character_id = character.character_id
        # Check if the api server is online
        if EveApiManager.check_if_api_server_online():
            fromID = wallet_transactions_add(character_id=character_id)
            print fromID
            while fromID != False:
                print 'Спускаемся ниже по транзакциям'
                fromID=wallet_transactions_add(character_id=character_id, fromID=fromID)

            fromID = wallet_journal_add(character_id=character_id)
            print fromID
            while fromID != False:
                print 'Спускаемся ниже по журналу'
                fromID=wallet_journal_add(character_id=character_id, fromID=fromID)

@periodic_task(run_every = timedelta (minutes = 31))
#Обновляем корп счета на основании корпоративного ключа
def updade_corps_wallets():
    apipars = EveApiKeyPair.objects.filter(is_corp = True)
    for api in apipars:
        api = (api.api_id, api.api_key)
        accounts = (1000, 1001, 1002, 1003, 1004, 1005, 1006)
        for account in accounts:
            fromID = wallet_transactions_corp(api=api, account=account, limit=2560, fromID = None)
            while fromID != False:
              #  print 'Спускаемся ниже по транзакциям'
                fromID = wallet_transactions_corp(api=api, account=account, limit=2560, fromID = fromID)

        for account in accounts:
            fromID = wallet_journal_corp(api=api, account=account, limit=2560, fromID = None)
            while fromID != False:
              #  print 'Спускаемся ниже по журналу'
                fromID = wallet_journal_corp(api=api, account=account, limit=2560, fromID = fromID)
