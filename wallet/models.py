# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from eveonline.models import EveCharacter, EveCorporationInfo, EveMapSovereignty, EveAllianceInfo, Divisions
from django.utils.translation import ugettext as _

# Create your models here.
from evestatic.models import MapSolarSystems


class EntryType(models.Model):
    """
    Wallet journal entry transaction type
    """
    refTypeID = models.PositiveIntegerField(primary_key=True)
    refTypeName = models.CharField(max_length=64)

    PLAYER_DONATION = 10
    CORP_WITHDRAWAL = 37
    BOUNTY_PRIZES = 85

    def __unicode__(self):
        return unicode(self.refTypeName)

class Jourmal(models.Model):
    result_id = models.BigIntegerField()
    result_date = models.DateTimeField()
    type_id = models.ForeignKey(EntryType, blank=False)
    party_1_name = models.CharField(max_length=254)
    party_1_id = models.BigIntegerField()
    party_1_type = models.CharField(max_length=254)
    party_2_name = models.CharField(max_length=254)
    party_2_id = models.BigIntegerField()
    party_2_type = models.CharField(max_length=254)
    arg_id = models.BigIntegerField()
    arg_name = models.CharField(max_length=254)
    result_amount = models.DecimalField(max_digits=28, decimal_places=2)
    result_balance = models.CharField(max_length=254)
    result_reason = models.CharField(max_length=254)
    tax_taxer_id = models.CharField(max_length=254)
    tax_amount = models.CharField(max_length=254)

    class Meta:
        abstract = True
        ordering = ['result_id']


class Transactions(models.Model):
    transactionID = models.BigIntegerField()
    transactionDateTime = models.DateTimeField()
    journalTransactionID = models.BigIntegerField()
    quantity = models.BigIntegerField(default = 0)
    typeID = models.IntegerField(default = 0)
    typeName = models.CharField(max_length=254)
    price = models.FloatField(default = 0.0)
    clientID = models.IntegerField()
    clientName = models.CharField(max_length=254)
    stationID = models.BigIntegerField(default = 0)
    stationName = models.CharField(max_length=254)
    transactionType = models.CharField(max_length=254)
    transactionFor = models.CharField(max_length=254)

    class Meta:
        abstract = True
        ordering = ['transactionID']


class CorpDivisions(Divisions):
    corporation = models.ForeignKey(EveCorporationInfo)

 #   def __str__(self):
 #        return self.description


class CharacterJournal(Jourmal):
    user = models.ForeignKey(User)
    character = models.ForeignKey(EveCharacter)

#    def __str__(self):
#        return self.character + '- Journal'


class CharacterTransactions(Transactions):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)
    character = models.ForeignKey(EveCharacter)

    def __str__(self):
        return self.character.character_name + '- Transactions'


class CorpJournal(Jourmal):
    account = models.IntegerField(blank=True, null=True,)
    corporation = models.ForeignKey(EveCorporationInfo)
    divisions = models.ForeignKey(CorpDivisions, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.corporation.corporation_name + '- Journal'


class CorpTransactions(Transactions):
    account = models.IntegerField(blank=True, null=True)
    corporation = models.ForeignKey(EveCorporationInfo)
    divisions = models.ForeignKey(CorpDivisions, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.corporation.corporation_name + '- Transactions'


class EveApiKeyCorp(models.Model):
    api_id = models.CharField(max_length=254)
    api_key = models.CharField(max_length=254)
    user = models.ForeignKey(User)
    corporation = models.ForeignKey(EveCorporationInfo)

    def __str__(self):
        return self.corporation.corporation_name +" - " + self.user.username + " - ApiKeyPairCorp"


class SystemsToRent(models.Model):
    system = models.ForeignKey(MapSolarSystems)
    holder = models.ForeignKey(EveAllianceInfo)
    price = models.DecimalField(max_digits=28, decimal_places=2, blank=True, null=True)
    payment_period = models.CharField(max_length=254)
    comment = models.TextField(blank=True, null=True)
    start_rent = models.DateField(blank=True, null=True)
    stop_rent = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.system.solar_system_name


class UserReportBounty(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    character = models.ForeignKey(EveCharacter)
    solar_system =  models.ForeignKey(SystemsToRent)
    summa = models.DecimalField(max_digits=28, decimal_places=2, verbose_name= _(u'Сумма заработка (баунти)'))
    # Если данные получены из корп налогов, то указываем какую корпорацию обработали
    #
    corp = models.ForeignKey(EveCorporationInfo, blank=True, null=True, verbose_name= _(u'Если считали по налогам корпорации, то указываем.'))
    date_month = models.IntegerField( verbose_name= _(u'Номер месяца, для которого считали сумму'))
    date_year = models.IntegerField( verbose_name= _(u'Год, для которого считали сумму'))
    created = models.DateTimeField(auto_now_add=True, verbose_name= _('Created Date/Time'))
    update = models.DateTimeField(auto_now=True, verbose_name= _('Last Update Date/Time'))
    # Сколько должен заплатить по тарифам
    amount_tax = models.DecimalField(max_digits=28, decimal_places=2, default=0, verbose_name= _(u'Сумма налога'))

    def __str__(self):
        return self.character.character_name