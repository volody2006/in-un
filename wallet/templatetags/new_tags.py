# -*- coding: utf-8 -*-
from alliance_auth import settings
from django.template import Library
from evemail.models import Notifications
from eveonline.models import EveCharacter

register = Library()

@register.simple_tag(takes_context=True)
def change_lang(context, lang=None, *args, **kwargs):
    """
    Get active page's url by a specified language
    Usage: {% change_lang 'en' %}
    """
    path = context['request'].path

    return '/{lang}/{path}'.format(lang=lang or settings.LANGUAGE_CODE,
                                   path=path[4:])



@register.simple_tag(takes_context=True)
def character(context, character_id, *args, **kwargs):
    try:
        return EveCharacter.objects.get(character_id=character_id)
    except:
        return None
    return None




@register.simple_tag(takes_context=True)
def notifications(context, *args, **kwargs):
    user= context['request'].user
#    for i in Notifications.objects.filter(user = user, read=False):
 #       print i.notification_id, i.sender_id
    return Notifications.objects.filter(user = user, read=False).count()