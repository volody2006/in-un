from django.contrib import admin
import reversion
# Register your models here.

from wallet.models import SystemsToRent, UserReportBounty, CorpDivisions, CorpJournal


class SystemsToRentAdmin(reversion.VersionAdmin):
    pass

class CorpDivisionsAdmin(reversion.VersionAdmin):
    list_filter = ['corporation' ]
    search_fields = ['description',
                     'corporation__corporation_name']
    list_display = ('account_key','description', 'corporation')


class UserReportBountyAdmin(reversion.VersionAdmin):
    list_filter = ['solar_system', 'date_year', 'date_month' ]
    search_fields = ['solar_system__system__solar_system_name',
                     'user__username', 'character__character_name', 'corp__corporation_name']
    list_display = ('character','user', 'solar_system',
                    'summa', 'amount_tax','date_year', 'date_month' ,'corp', 'created', 'update')


admin.site.register(UserReportBounty, UserReportBountyAdmin)
admin.site.register(SystemsToRent, SystemsToRentAdmin)
#admin.site.register(CorpDivisions, CorpDivisionsAdmin)

#admin.site.register(CorpJournal)