# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wallet', '0002_auto_20150808_1424'),
    ]

    operations = [
        migrations.AlterField(
            model_name='characterjournal',
            name='result_amount',
            field=models.DecimalField(max_digits=28, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='corpjournal',
            name='result_amount',
            field=models.DecimalField(max_digits=28, decimal_places=2),
        ),
    ]
