# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('eveonline', '0007_auto_20150813_0437'),
        ('wallet', '0003_auto_20150810_0611'),
    ]

    operations = [
        migrations.CreateModel(
            name='CorpDivisions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('account_key', models.IntegerField()),
                ('description', models.CharField(max_length=254)),
                ('corporation', models.ForeignKey(to='eveonline.EveCorporationInfo')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterField(
            model_name='corpjournal',
            name='account',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='corptransactions',
            name='account',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='userreportbounty',
            name='corp',
            field=models.ForeignKey(verbose_name='\u0415\u0441\u043b\u0438 \u0441\u0447\u0438\u0442\u0430\u043b\u0438 \u043f\u043e \u043d\u0430\u043b\u043e\u0433\u0430\u043c \u043a\u043e\u0440\u043f\u043e\u0440\u0430\u0446\u0438\u0438, \u0442\u043e \u0443\u043a\u0430\u0437\u044b\u0432\u0430\u0435\u043c.', blank=True, to='eveonline.EveCorporationInfo', null=True),
        ),
        migrations.AlterField(
            model_name='userreportbounty',
            name='date_month',
            field=models.IntegerField(verbose_name='\u041d\u043e\u043c\u0435\u0440 \u043c\u0435\u0441\u044f\u0446\u0430, \u0434\u043b\u044f \u043a\u043e\u0442\u043e\u0440\u043e\u0433\u043e \u0441\u0447\u0438\u0442\u0430\u043b\u0438 \u0441\u0443\u043c\u043c\u0443'),
        ),
        migrations.AlterField(
            model_name='userreportbounty',
            name='date_year',
            field=models.IntegerField(verbose_name='\u0413\u043e\u0434, \u0434\u043b\u044f \u043a\u043e\u0442\u043e\u0440\u043e\u0433\u043e \u0441\u0447\u0438\u0442\u0430\u043b\u0438 \u0441\u0443\u043c\u043c\u0443'),
        ),
        migrations.AlterField(
            model_name='userreportbounty',
            name='summa',
            field=models.DecimalField(verbose_name='\u0421\u0443\u043c\u043c\u0430 \u0437\u0430\u0440\u0430\u0431\u043e\u0442\u043a\u0430 (\u0431\u0430\u0443\u043d\u0442\u0438)', max_digits=28, decimal_places=2),
        ),
        migrations.AddField(
            model_name='corpjournal',
            name='divisions',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='wallet.CorpDivisions', null=True),
        ),
        migrations.AddField(
            model_name='corptransactions',
            name='divisions',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='wallet.CorpDivisions', null=True),
        ),
    ]
