# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wallet', '0004_auto_20150820_1723'),
    ]

    operations = [
        migrations.AddField(
            model_name='userreportbounty',
            name='amount_tax',
            field=models.DecimalField(default=0, verbose_name='\u0421\u0443\u043c\u043c\u0430 \u043d\u0430\u043b\u043e\u0433\u0430', max_digits=28, decimal_places=2),
        ),
    ]
