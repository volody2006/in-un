# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('eveonline', '0002_evecharacter_id'),
        ('evestatic', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='CharacterJournal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('result_id', models.BigIntegerField()),
                ('result_date', models.DateTimeField()),
                ('party_1_name', models.CharField(max_length=254)),
                ('party_1_id', models.BigIntegerField()),
                ('party_1_type', models.CharField(max_length=254)),
                ('party_2_name', models.CharField(max_length=254)),
                ('party_2_id', models.BigIntegerField()),
                ('party_2_type', models.CharField(max_length=254)),
                ('arg_id', models.BigIntegerField()),
                ('arg_name', models.CharField(max_length=254)),
                ('result_amount', models.DecimalField(max_digits=28, decimal_places=4)),
                ('result_balance', models.CharField(max_length=254)),
                ('result_reason', models.CharField(max_length=254)),
                ('tax_taxer_id', models.CharField(max_length=254)),
                ('tax_amount', models.CharField(max_length=254)),
                ('character', models.ForeignKey(to='eveonline.EveCharacter')),
            ],
            options={
                'ordering': ['result_id'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CharacterTransactions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('transactionID', models.BigIntegerField()),
                ('transactionDateTime', models.DateTimeField()),
                ('journalTransactionID', models.BigIntegerField()),
                ('quantity', models.BigIntegerField(default=0)),
                ('typeID', models.IntegerField(default=0)),
                ('typeName', models.CharField(max_length=254)),
                ('price', models.FloatField(default=0.0)),
                ('clientID', models.IntegerField()),
                ('clientName', models.CharField(max_length=254)),
                ('stationID', models.BigIntegerField(default=0)),
                ('stationName', models.CharField(max_length=254)),
                ('transactionType', models.CharField(max_length=254)),
                ('transactionFor', models.CharField(max_length=254)),
                ('character', models.ForeignKey(to='eveonline.EveCharacter')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['transactionID'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CorpJournal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('result_id', models.BigIntegerField()),
                ('result_date', models.DateTimeField()),
                ('party_1_name', models.CharField(max_length=254)),
                ('party_1_id', models.BigIntegerField()),
                ('party_1_type', models.CharField(max_length=254)),
                ('party_2_name', models.CharField(max_length=254)),
                ('party_2_id', models.BigIntegerField()),
                ('party_2_type', models.CharField(max_length=254)),
                ('arg_id', models.BigIntegerField()),
                ('arg_name', models.CharField(max_length=254)),
                ('result_amount', models.DecimalField(max_digits=28, decimal_places=4)),
                ('result_balance', models.CharField(max_length=254)),
                ('result_reason', models.CharField(max_length=254)),
                ('tax_taxer_id', models.CharField(max_length=254)),
                ('tax_amount', models.CharField(max_length=254)),
                ('account', models.IntegerField()),
                ('corporation', models.ForeignKey(to='eveonline.EveCorporationInfo')),
            ],
            options={
                'ordering': ['result_id'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CorpTransactions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('transactionID', models.BigIntegerField()),
                ('transactionDateTime', models.DateTimeField()),
                ('journalTransactionID', models.BigIntegerField()),
                ('quantity', models.BigIntegerField(default=0)),
                ('typeID', models.IntegerField(default=0)),
                ('typeName', models.CharField(max_length=254)),
                ('price', models.FloatField(default=0.0)),
                ('clientID', models.IntegerField()),
                ('clientName', models.CharField(max_length=254)),
                ('stationID', models.BigIntegerField(default=0)),
                ('stationName', models.CharField(max_length=254)),
                ('transactionType', models.CharField(max_length=254)),
                ('transactionFor', models.CharField(max_length=254)),
                ('account', models.IntegerField()),
                ('corporation', models.ForeignKey(to='eveonline.EveCorporationInfo')),
            ],
            options={
                'ordering': ['transactionID'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='EntryType',
            fields=[
                ('refTypeID', models.PositiveIntegerField(serialize=False, primary_key=True)),
                ('refTypeName', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='EveApiKeyCorp',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('api_id', models.CharField(max_length=254)),
                ('api_key', models.CharField(max_length=254)),
                ('corporation', models.ForeignKey(to='eveonline.EveCorporationInfo')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='SystemsToRent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.DecimalField(null=True, max_digits=28, decimal_places=2, blank=True)),
                ('payment_period', models.CharField(max_length=254)),
                ('comment', models.TextField(null=True, blank=True)),
                ('start_rent', models.DateField(null=True, blank=True)),
                ('stop_rent', models.DateField(null=True, blank=True)),
                ('holder', models.ForeignKey(to='eveonline.EveAllianceInfo')),
                ('system', models.ForeignKey(to='evestatic.MapSolarSystems')),
            ],
        ),
        migrations.CreateModel(
            name='UserReportBounty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('summa', models.DecimalField(max_digits=28, decimal_places=2)),
                ('date_month', models.IntegerField()),
                ('date_year', models.IntegerField()),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Created Date/Time')),
                ('update', models.DateTimeField(auto_now=True, verbose_name=b'Last Update Date/Time')),
                ('character', models.ForeignKey(to='eveonline.EveCharacter')),
                ('corp', models.ForeignKey(blank=True, to='eveonline.EveCorporationInfo', null=True)),
                ('solar_system', models.ForeignKey(to='wallet.SystemsToRent')),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='corpjournal',
            name='type_id',
            field=models.ForeignKey(to='wallet.EntryType'),
        ),
        migrations.AddField(
            model_name='characterjournal',
            name='type_id',
            field=models.ForeignKey(to='wallet.EntryType'),
        ),
        migrations.AddField(
            model_name='characterjournal',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
