# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wallet', '0005_auto_20150820_0515'),
        ('wallet', '0005_userreportbounty_amount_tax'),
    ]

    operations = [
    ]
