# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wallet', '0004_auto_20150820_1723'),
    ]

    operations = [
        migrations.AddField(
            model_name='corpjournal',
            name='divisions',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='wallet.CorpDivisions', null=True),
        ),
        migrations.AddField(
            model_name='corptransactions',
            name='divisions',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='wallet.CorpDivisions', null=True),
        ),
    ]
