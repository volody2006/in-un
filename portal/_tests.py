# Create your tests here.
import unittest
from django.test import Client, TestCase
from test.util import TestBase


class HomePageTest(TestBase):
  #  fixtures = ['data.json']

    def test_homepage_available(self):
        c = Client()
        #response = c.post('/login/', {'username': 'john', 'password': 'smith'})
        response = c.get('/')
        self.assertEquals(response.status_code, 200)

        response = c.get('/login_user/')
        self.assertEquals(response.status_code, 302)





