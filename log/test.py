# -*- coding: utf-8 -*-
from decimal import Decimal

__author__ = 'user'

{'cashflow': Decimal('-2100000000.0000'),
 'expenditure_time':
     [{'date': datetime.datetime(2015, 6, 24, 0, 0, 0, 990704, tzinfo=<UTC>),
'result_amount': 0},
{'date': datetime.datetime(2015, 6, 25, 0, 0, 0, 990704, tzinfo=<UTC>),
'result_amount': 0},
{'date': datetime.datetime(2015, 6, 26, 0, 0, 0, 990704, tzinfo=<UTC>),
'result_amount': 0}, {'date': datetime.datetime(2015, 6, 27, 0, 0, 0, 990704, tzinfo=<UTC>),
'result_amount': 0},
{'date': datetime.datetime(2015, 6, 28, 0, 0, 0, 990704, tzinfo=<UTC>),
'result_amount': 0},
{'date': datetime.datetime(2015, 6, 29, 0, 0, 0, 990704, tzinfo=<UTC>),
'result_amount': 0},
{'date': datetime.datetime(2015, 6, 30, 0, 0, 0, 990704, tzinfo=<UTC>),
'result_amount': Decimal('-9000000000.0000')},
{'date': datetime.datetime(2015, 7, 1, 0, 0, 0, 990704, tzinfo=<UTC>),
'result_amount': 0},
{'date': datetime.datetime(2015, 7, 2, 0, 0, 0, 990704, tzinfo=<UTC>),
'result_amount': 0},
{'date': datetime.datetime(2015, 7, 3, 0, 0, 0, 990704, tzinfo=<UTC>),
'result_amount': 0},
{'date': datetime.datetime(2015, 7, 4, 0, 0, 0, 990704, tzinfo=<UTC>),
'result_amount': Decimal('-150010000.0000')},
{'date': datetime.datetime(2015, 7, 5, 0, 0, 0, 990704, tzinfo=<UTC>),
'result_amount': Decimal('-8990000.0000')}, {'date': datetime.datetime(2015, 7, 6, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 7, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 8, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 9, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 10, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 11, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 12, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 13, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 14, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 15, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 16, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 17, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 18, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 19, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 20, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 21, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0},
{'date': datetime.datetime(2015, 7, 22, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 23, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}],
'expenditure_total': Decimal('-9159000000.0000'), 'income_total': Decimal('7059000000.0000'), 'income_aggregated': [{'type_id_id': 10, 'percentage': Decimal('99.87250318742031449213769656'), 'result_amount': Decimal('7050000000.0000')}, {'type_id_id': 71, 'percentage': Decimal('0.1274968125796855078623034424'), 'result_amount': Decimal('9000000.0000')}], 'income_time':
[{'date': datetime.datetime(2015, 6, 24, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': Decimal('450 000 000.0000')}, {'date': datetime.datetime(2015, 6, 25, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 6, 26, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 6, 27, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': Decimal('300000000.0000')}, {'date': datetime.datetime(2015, 6, 28, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': Decimal('4800000000.0000')}, {'date': datetime.datetime(2015, 6, 29, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': Decimal('130000000.0000')}, {'date': datetime.datetime(2015, 6, 30, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': Decimal('750000000.0000')}, {'date': datetime.datetime(2015, 7, 1, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': Decimal('20000000.0000')}, {'date': datetime.datetime(2015, 7, 2, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 3, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': Decimal('150000000.0000')}, {'date': datetime.datetime(2015, 7, 4, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': Decimal('9000000.0000')}, {'date': datetime.datetime(2015, 7, 5, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 6, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 7, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 8, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 9, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 10, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': Decimal('300000000.0000')}, {'date': datetime.datetime(2015, 7, 11, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 12, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 13, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 14, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 15, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 16, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 17, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': Decimal('150000000.0000')}, {'date': datetime.datetime(2015, 7, 18, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 19, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 20, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 21, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 22, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}, {'date': datetime.datetime(2015, 7, 23, 0, 0, 0, 990704, tzinfo=<UTC>), 'result_amount': 0}],
'expenditure_aggregated':
[{'type_id_id': 37,
  'percentage': Decimal('99.99989081777486625177421116'),
  'result_amount': Decimal('-9158990000.0000')},
 {'type_id_id': 80,
  'percentage': Decimal('0.0001091822251337482257888415766'),
  'result_amount': Decimal('-10000.0000')}]}
