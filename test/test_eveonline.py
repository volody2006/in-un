# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.test import Client
from evelink.api import APIResult
from eveonline.forms import UpdateKeyForm
from eveonline.managers import EveManager
from eveonline.models import EveCharacter, EveApiKeyPair
import mock

from test.util import TestBase


class AddApiKeyTest(TestBase):
    fixtures = ['fixtures/eveonline_fix.json']

    def setUp(self):
        super(AddApiKeyTest, self).setUp()
        self.url = reverse('auth_add_api_key')


    def test_anonymous_add_api(self):
        """
        Анонимный пользователь при попытке захода получает переадресацию на логин
        """
        self.became_anonymous()
        self.assertEquals(302, self.client.get(self.url).status_code)

    def test_user_add_api(self):
        data = self.api_valid_full()
        with mock.patch(
                'services.managers.eve_api_manager.EveApiManager.check_api_is_valid',
                return_value = self.check_api_is_valid(data['api_id'])
        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.get_api_info',
            return_value = self.get_api_info(data['api_id'])

        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.get_characters_from_api',
            return_value = self.get_characters_from_api(data['api_id'])
        ):

            page = self.client.get(self.url)
            self.assertEquals(200, page.status_code)
            self.assertTemplateUsed(page, 'dashboard/eveonline/addapikey.html')
            self.assertIn('form', page.context)

            # И засылаем форму
            data = self.api_valid_full()
            data['is_blue'] = False


            page = self.client.post(self.url, data)

            api = EveApiKeyPair.objects.get(api_id = data['api_id'])

            # Пользователь нормально поставился?
            self.assertEquals(api.user, self.user)
            self.assertEqual(str(api), self.user.username + " - ApiKeyPair")

            # Чары загрузились?
            char = EveCharacter.objects.get(character_name = 'Lili Cadelanne')
            self.assertEquals(char.character_name, 'Lili Cadelanne')

            # Чарам апи присвоен?
            self.assertEquals(int(char.api_id), int(api.api_id))

            # Чары юзеру присвоились?
            self.assertEquals(char.user, self.user)

    def test_user_add_api_blue(self):
        page = self.client.get(self.url)

        # И засылаем форму
        data = self.api_valid_limit()
        data['is_blue'] = True


        page = self.client.post(self.url, data)

        api = EveApiKeyPair.objects.get(api_id = data['api_id'])

        # Пользователь нормально поставился?
        self.assertEquals(api.user, self.user)

    def test_user_add_api_bad(self):
        with mock.patch(
                'services.managers.eve_api_manager.EveApiManager.check_api_is_valid',
                return_value = False):

            data = self.api_no_valid()
            data['is_blue'] = False

            form = UpdateKeyForm(data)

            self.assertFalse(form.is_valid())
       # self.assertFormError(UpdateKeyForm, 'form', None, u'API key is invalid. Please make another.')

    def test_user_add_api_corp(self):
        data = self.api_valid_corp()
        with mock.patch(
                'services.managers.eve_api_manager.EveApiManager.check_api_is_valid',
                return_value = self.check_api_is_valid(data['api_id'])

        ), mock.patch('services.managers.eve_api_manager.EveApiManager.get_api_info',
            return_value = self.get_api_info(data['api_id'])

        ), mock.patch('services.managers.eve_api_manager.EveApiManager.get_characters_from_api',
            return_value = self.get_characters_from_api(data['api_id'])
        ):

            page = self.client.get(self.url)
            self.assertEquals(200, page.status_code)
            self.assertTemplateUsed(page, 'dashboard/eveonline/addapikey.html')
            self.assertIn('form', page.context)

            # И засылаем форму
            data = self.api_valid_corp()
            data['is_blue'] = False


            page = self.client.post(self.url, data)

            api = EveApiKeyPair.objects.get(api_id = data['api_id'])

            # Пользователь нормально поставился?
            self.assertEquals(api.user, self.user)

            # Статус корп ключа стоит?
            self.assertTrue(api.is_corp)

            # Корпорация прописана?

            self.assertEqual(int(api.corporation.corporation_id), 98368686)

    def test_user_del_api(self):
        self.test_user_add_api()
        api = EveApiKeyPair.objects.get(api_id = self.api_valid_full()['api_id'])
        self.url = reverse('auth_api_key_management')
        page = self.client.get(self.url)
        self.assertEquals(200, page.status_code)
        self.assertTemplateUsed(page, 'dashboard/eveonline/apikeymanagment.html')

        self.assertEquals(page.context['apikeypairs'][0].api_id, EveManager.get_api_key_pairs(self.user.id)[0].api_id)

        self.url = reverse('auth_api_key_removal', args= [page.context['apikeypairs'][0].api_id])
        page = self.client.get(self.url)
        self.assertEquals(302, page.status_code)

        api = EveApiKeyPair.objects.get(api_id = self.api_valid_full()['api_id'])
        self.assertEqual(str(api), 'None - ApiKeyPair')

        char = EveCharacter.objects.get(character_name = 'Lili Cadelanne')
        self.assertIsNone(char.user)

class MainCharTest(TestBase):

    def setUp(self):
        super(MainCharTest, self).setUp()

    def characters_view_test(self):
        AddApiKeyTest.test_user_add_api()
        self.url = reverse('auth_characters')
        page = self.client.get(self.url)
        self.assertEquals(200, page.status_code)
        self.assertTemplateUsed(page, 'dashboard/eveonline/characters.html')

class EveManagerTest(TestBase):

    def setUp(self):
        super(EveManagerTest, self).setUp()


    def test_delete_api_key_pair(self):
        user = self.user
        char = EveCharacter.objects.create(character_id = 93840640,
                                           character_name = 'Lili Cadelanne',
                                           corporation_id = 98292612,
                                           corporation_name = 'Stalin Corporation',
                                           alliance_id = 99005103,
                                           alliance_name = 'Independent Union',
                                           api_id = 123456,
                                           user = user
                                           )

        self.assertEqual(char.character_id, 93840640)
        EveManager.delete_characters_by_api_id(char.api_id, char.user.id)

        char = EveCharacter.objects.get(pk=char.pk)
        self.assertIsNone(char.user)
        self.assertIsNone(char.api_id)

    def test_update_or_create_character_base(self):
        with mock.patch('evelink.eve.EVE.character_info_from_id') as mock_is:
            mock_is.return_value = self.evelink_character_info_from_id(93840640)
            obj = EveManager.update_or_create_character_base(93840640)
            self.assertEqual(obj.character_name, 'Lili Cadelanne')

    def test_update_or_create_corporation_base_bad(self):
        with mock.patch('eveonline.managers.EveManager.update_or_create_character_base', return_value = False):
            obj  = EveManager.update_or_create_corporation_base(98292612)
            self.assertEqual(obj, False)


    def test_update_or_create_corporation_base(self):
        with mock.patch(
                'services.managers.eve_api_manager.EveApiManager.get_corporation_information',
                return_value = self.get_corporation_information(98292612)

        ), mock.patch(
            'evelink.eve.EVE.character_info_from_id',
            return_value = self.evelink_character_info_from_id(93959323)

        ), mock.patch(
            'services.managers.eve_api_manager.EveApiManager.get_alliance_information',
            return_value = self.get_alliance_information(99005103)
        ):

            obj  = EveManager.update_or_create_corporation_base(98292612)
            self.assertEqual(obj.corporation_id, 98292612)

    def test_update_or_create_alliance_info(self):
        with mock.patch(
            'services.managers.eve_api_manager.EveApiManager.get_alliance_information',
            return_value = self.get_alliance_information(99005103)
        ):
            obj = EveManager.update_or_create_alliance_info(99005103)
            self.assertEqual(obj.alliance_id, 99005103)

    def test_create_character(self):
        EveManager.create_character(
            character_id = 123456,
            character_name = 'Test Test',
            corporation_id = 456789,
            corporation_name = 'Corp Test',
            corporation_ticker = 'QWER',
            alliance_id = 789456,
            alliance_name = 'Ali Test',
            user = self.user,
            api_id = 741258
        )
        char = EveCharacter.objects.get(pk=123456)
        self.assertEqual(char.user, self.user)
        self.assertEqual(char.character_id, 123456)

    def test_create_characters_from_list(self):
        with mock.patch(
            'services.managers.eve_api_manager.EveApiManager.get_corporation_ticker_from_id',
            return_value = self.get_corporation_ticker_from_id(98292612)
        ):

            chars = self.get_characters_from_api(4641111)
            EveManager.create_characters_from_list(
                chars = chars,
                user = self.user,
                api_id = 4641111
            )

            chars = EveCharacter.objects.filter(api_id=4641111)
            self.assertEqual(chars.count(), 3)

            char = EveCharacter.objects.get(pk=93840640)
            self.assertEqual(char.user, self.user)
            self.assertEqual(char.character_id, 93840640)
            self.assertEqual(char.corporation_ticker, 'SVC-S')

            char = EveCharacter.objects.get(pk=95208193)
            self.assertEqual(char.user, self.user)
            self.assertEqual(char.character_id, 95208193)
            self.assertEqual(char.corporation_ticker, 'SVC-S')

            char = EveCharacter.objects.get(pk=94541861)
            self.assertEqual(char.user, self.user)
            self.assertEqual(char.character_id, 94541861)
            self.assertEqual(char.corporation_ticker, 'SVC-S')

    def test_update_characters_from_list(self):
        self.test_create_characters_from_list()
        with mock.patch(
            'services.managers.eve_api_manager.EveApiManager.get_corporation_ticker_from_id',
            return_value = self.get_corporation_ticker_from_id(98368686)
        ):
            chars = self.get_characters_from_api(4641111)
            EveManager.update_characters_from_list(chars)

            chars = EveCharacter.objects.filter(api_id=4641111)
            self.assertEqual(chars.count(), 3)

            char = EveCharacter.objects.get(pk=93840640)
            self.assertEqual(char.user, self.user)
            self.assertEqual(char.character_id, 93840640)
            self.assertEqual(char.corporation_ticker, 'GHBDT')

            char = EveCharacter.objects.get(pk=95208193)
            self.assertEqual(char.user, self.user)
            self.assertEqual(char.character_id, 95208193)
            self.assertEqual(char.corporation_ticker, 'GHBDT')

            char = EveCharacter.objects.get(pk=94541861)
            self.assertEqual(char.user, self.user)
            self.assertEqual(char.character_id, 94541861)
            self.assertEqual(char.corporation_ticker, 'GHBDT')
