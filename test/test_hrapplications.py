# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from eveonline.models import  EveAllianceInfo, EveCharacter
from hrapplications.models import HRApplication

import mock
from test.util import TestBase

__author__ = 'volody'


class TestHR_no_fixture(TestBase):

    def setUp(self):

        super(TestHR_no_fixture, self).setUp()
        self.url = reverse('auth_hrapplication_create_view')


    def test_hr_application_create_view_no_fix(self):
        self.login_user()
        page = self.client.get(self.url)
        self.assertEquals(200, page.status_code)
        #self.assertTemplateUsed(page, 'dashboard/hrapplications/hrcreateapplication.html')
        from hrapplications.forms import HRApplicationForm
        form = HRApplicationForm()


class TestHR(TestBase):

    fixtures = ['fixtures/eveonline_fix.json']

    def setUp(self):

        super(TestHR, self).setUp()
        self.url = reverse('auth_hrapplication_create_view')

    def test_anonymous_hr(self):
        """
        Анонимный пользователь при попытке захода получает переадресацию на логин
        """
        self.became_anonymous()
        self.assertEquals(302, self.client.get(self.url).status_code)

    def test_hr_application_create_view(self):
        self.login_user()
        page = self.client.get(self.url)
        self.assertEquals(200, page.status_code)
        #self.assertTemplateUsed(page, 'dashboard/hrapplications/hrcreateapplication.html')

    def test_hr_application_create_view_good(self):
        data_api = self.api_valid_full()

        with mock.patch(
                        'services.managers.eve_api_manager.EveApiManager.check_api_is_valid',
                         return_value = self.check_api_is_valid(data_api['api_id'])
        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.check_api_is_type_account',
                         return_value = self.check_api_is_type_account(data_api['api_id'])

        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.get_characters_from_api',
                         return_value = self.get_characters_from_api(data_api['api_id'])
        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.check_api_is_full',
                         return_value = True
        ):

            page = self.client.get(self.url)

            self.assertEquals(200, page.status_code)
            #self.assertTemplateUsed(page, 'dashboard/hrapplications/hrcreateapplication.html')
            self.assertIn('form', page.context)

            # И засылаем форму
            data = {}
            data['character_name'] = 'Lili Cadelanne'
            data['full_api_id'] = data_api['api_id']
            data['full_api_key'] = data_api['api_key']
            data['corp'] = '98292612'
            data['is_a_spi'] = 'Yes'
            data['about'] = 'Test about'
            data['extra'] = 'Test extra'


            from hrapplications.forms import HRApplicationForm

            form = HRApplicationForm(data)
            #print HRApplicationForm()
            self.assertTrue(form.is_valid())

            page = self.client.post(self.url, data)

            hr1 = HRApplication.objects.get(full_api_id = data['full_api_id'])

            self.assertEquals(hr1.user, self.user)
            self.assertEqual(str(hr1), 'Lili Cadelanne' + " - Application")
            #
            # Чары загрузились?
            char = EveCharacter.objects.get(character_name = 'Lili Cadelanne')
            self.assertEquals(char.character_name, 'Lili Cadelanne')
            #
            # Чарам апи присвоен?
            self.assertEquals(int(char.api_id), int(data_api['api_id']))
            #
            # Чары юзеру присвоились?
            self.assertEquals(char.user, self.user)

    def test_hr_application_create_view_bad_no_full_api(self):
        data_api = self.api_valid_full()

        with mock.patch(
                'services.managers.eve_api_manager.EveApiManager.check_api_is_valid',
                return_value = self.check_api_is_valid(data_api['api_id'])
        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.check_api_is_type_account',
            return_value = self.check_api_is_type_account(data_api['api_id'])

        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.get_characters_from_api',
            return_value = self.get_characters_from_api(data_api['api_id'])
        ), mock.patch('services.managers.eve_api_manager.EveApiManager.check_api_is_full',
                       return_value = False
        ):


            # И засылаем форму
            data = {}
            data['character_name'] = 'Lili Cadelanne'
            data['full_api_id'] = data_api['api_id']
            data['full_api_key'] = data_api['api_key']
            data['corp'] = '98292612'
            data['is_a_spi'] = 'Yes'
            data['about'] = 'Test about'
            data['extra'] = 'Test extra'
            from hrapplications.forms import HRApplicationForm
            form = HRApplicationForm(data)

            self.assertFalse(form.is_valid())


    def test_hr_application_create_view_bad_valid_limit_api(self):
        # Проверяем, что это ключ, а не набор случайных данных
        data_api = self.api_no_valid()

        with mock.patch('services.managers.eve_api_manager.EveApiManager.check_api_is_valid',
                         return_value = self.check_api_is_valid(data_api['api_id'])
        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.check_api_is_type_account',
                         return_value = self.check_api_is_type_account(data_api['api_id'])
        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.get_characters_from_api',
                         return_value = self.get_characters_from_api(data_api['api_id'])
        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.check_api_is_full',
                        return_value = False
        ):

    #         И засылаем форму
            data = {}
            data['character_name'] = 'Lili Cadelanne'
            data['full_api_id'] = data_api['api_id']
            data['full_api_key'] = data_api['api_key']
            data['corp'] = '98292612'
            data['is_a_spi'] = 'Yes'
            data['about'] = 'Test about'
            data['extra'] = 'Test extra'
            from hrapplications.forms import HRApplicationForm

            form = HRApplicationForm(data)
            # print form

            self.assertFalse(form.is_valid())

    def test_hr_application_create_view_bad_corp_api(self):
        # Проверяем тип ключа
        data_api = self.api_valid_corp()

        with mock.patch(
                'services.managers.eve_api_manager.EveApiManager.check_api_is_valid',
                return_value = self.check_api_is_valid(data_api['api_id'])
        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.check_api_is_type_account',
            return_value = self.check_api_is_type_account(data_api['api_id'])

        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.get_characters_from_api',
            return_value = self.get_characters_from_api(data_api['api_id'])
        ), mock.patch('services.managers.eve_api_manager.EveApiManager.check_api_is_full',
                       return_value = True
                      ):

            # И засылаем форму
            data = {}
            data['character_name'] = 'Lili Cadelanne'
            data['full_api_id'] = data_api['api_id']
            data['full_api_key'] = data_api['api_key']
            data['corp'] = '98292612'
            data['is_a_spi'] = 'Yes'
            data['about'] = 'Test about'
            data['extra'] = 'Test extra'
            from hrapplications.forms import HRApplicationForm

            form = HRApplicationForm(data)

            self.assertFalse(form.is_valid())

    def test_hr_application_create_view_bad_no_valid_char(self):
        # Персонаж  не соответсвует апи ключу
        data_api = self.api_valid_full()

        with mock.patch(
                'services.managers.eve_api_manager.EveApiManager.check_api_is_valid',
                return_value = self.check_api_is_valid(data_api['api_id'])
        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.check_api_is_type_account',
            return_value = self.check_api_is_type_account(data_api['api_id'])

        ),   mock.patch('services.managers.eve_api_manager.EveApiManager.get_characters_from_api',
            return_value = self.get_characters_from_api(data_api['api_id'])
        ), mock.patch('services.managers.eve_api_manager.EveApiManager.check_api_is_full',
                       return_value = True
                      ):

            # И засылаем форму
            data = {}
            data['character_name'] = 'Rediska'
            data['full_api_id'] = data_api['api_id']
            data['full_api_key'] = data_api['api_key']
            data['corp'] = '98292612'
            data['is_a_spi'] = 'Yes'
            data['about'] = 'Test about'
            data['extra'] = 'Test extra'
            from hrapplications.forms import HRApplicationForm

            form = HRApplicationForm(data)

            self.assertFalse(form.is_valid())

    def test_hr_application_create_view_bad_no_api(self):
        # поле вввода ключа пустое.
        data = {}
        data['character_name'] = 'Rediska'
       # data['full_api_id'] = data_api['api_id']
     #   data['full_api_key'] = data_api['api_key']
        data['corp'] = '98292612'
        data['is_a_spi'] = 'Yes'
        data['about'] = 'Test about'
        data['extra'] = 'Test extra'
        from hrapplications.forms import HRApplicationForm

        form = HRApplicationForm(data)

        self.assertFalse(form.is_valid())
