from django import forms
from django.utils.translation import ugettext as _

class SrpFleetMainForm(forms.Form):
    fleet_name = forms.CharField(required=True, label=_('Fleet Name'))
    fleet_time = forms.DateTimeField(required=True, label=_('Fleet Time'))
    fleet_doctrine = forms.CharField(required=True, label=_('Fleet Doctrine'))


class SrpFleetUserRequestForm(forms.Form):
    killboard_link = forms.CharField(required=True, label=_('Killboard Link'))
    additional_info = forms.CharField(required=False, label=_('Additional Info'))


class SrpFleetUpdateCostForm(forms.Form):
    srp_total_amount = forms.IntegerField(required=True, label=_('Total SRP Amount'))


class SrpFleetMainUpdateForm(forms.Form):
    fleet_aar_link = forms.CharField(required=True, label=_('After Action Report Link'))