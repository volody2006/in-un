"""
Django settings for alliance_auth project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

import djcelery
#from django.utils.translation import ugettext as _



djcelery.setup_loader()
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('AA_SECRET_KEY', '%-&xo)f9vfuet*r$eob8q3!1fsw$t0&e0f1b0-5ou56#zw&_m4')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['in-un.ru', 'www.in-un.ru']

ADMINS = (('Vladimir', 'senokosov.vs@gmail.com'), )

#BROKER_URL = 'amqp://guest:guest@localhost:5672/'

#CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"

# Redis

BROKER_HOST = "localhost"
BROKER_BACKEND="redis"
REDIS_PORT=6379
REDIS_HOST = "localhost"
BROKER_USER = ""
BROKER_PASSWORD =""
BROKER_VHOST = "0"
REDIS_DB = 0
REDIS_CONNECT_RETRY = True
CELERY_SEND_EVENTS=True
CELERY_RESULT_BACKEND='redis'
CELERY_TASK_RESULT_EXPIRES =  10
CELERYBEAT_SCHEDULER="djcelery.schedulers.DatabaseScheduler"
CELERY_ALWAYS_EAGER=False
# #



# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'rosetta',
    'reversion',
    'djcelery',
    'celerytask',
    'bootstrapform',
    'authentication',
    'portal',
    'registration',
    'services',
    'eveonline',
    'groupmanagement',
    'hrapplications',
    'timerboard',
    'srp',
    'wallet',
    'history',
    'evestatic',
    'evemail',
    'balance',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

)

ROOT_URLCONF = 'alliance_auth.urls'

WSGI_APPLICATION = 'alliance_auth.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {

    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'alliance_auth',
        'USER': 'allianceauth',
        'PASSWORD': 'cIvTz9Fbyw',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    },

    'phpbb3': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'alliance_forum',
        'USER': 'allianceauth',
        'PASSWORD': 'cIvTz9Fbyw',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    },

    'mumble': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'alliance_mumble',
        'USER': 'alliancemumble',
        'PASSWORD': 'cIvTz9Fbyw',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    },


}
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',
    'util.context_processors.alliance_id',
    'util.context_processors.alliance_name',
    'util.context_processors.alliance_tiker',
    'util.context_processors.jabber_url',
    'util.context_processors.domain_url',
    'django.template.context_processors.csrf',
    'util.context_processors.user_characters',
    'util.context_processors.notifications',
)

TEMPLATE_DIRS = (
    'templates',
)

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
    'static',
)

LOGIN_URL = '/login_user/'

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'ru'
#LANGUAGE_CODE = 'en-US'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ('ru', ('Russian')),
    ('en', ('English')),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)
# Rosetta
YANDEX_TRANSLATE_KEY = 'trnsl.1.1.20150604T103608Z.1b07026be52e874f.3ed98a8fcf0da87879113a03a507e0be03b5ea50'
ROSETTA_MESSAGES_PER_PAGE = 20
#ROSETTA_GOOGLE_TRANSLATE = True
ROSETTA_ENABLE_TRANSLATION_SUGGESTIONS = True
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATIC_URL = '/static/'

#####################################################
##
## Alliance configuration starts here
##
#####################################################

#################
# EMAIL SETTINGS
#################
# DOMAIN - The alliance auth domain_url
# EMAIL_HOST - SMTP Server URL
# EMAIL_PORT - SMTP Server PORT
# EMAIL_HOST_USER - Email Username
# EMAIL_HOST_PASSWORD - Email Password
# EMAIL_USE_TLS - Set to use TLS encryption
#################
DOMAIN = 'http://in-un.ru' #os.environ.get('AA_DOMAIN', 'http://in-un.ru')
EMAIL_HOST = 'smtp.yandex.ru' # os.environ.get('AA_EMAIL_HOST', 'smtp.gmail.com')
EMAIL_PORT = 587 #int(os.environ.get('AA_EMAIL_PORT', '587'))
EMAIL_HOST_USER = 'webmaster@in-un.ru' #os.environ.get('AA_EMAIL_HOST_USER', 'netotveta.inun@gmail.com') #v.senokosov@yandex.ru netotveta.inun@gmail.com
EMAIL_HOST_PASSWORD = 'Eshemoba1' # os.environ.get('AA_EMAIL_HOST_PASSWORD', 'Eshemoba1')
EMAIL_USE_TLS = 'True' # == os.environ.get('AA_EMAIL_USE_TLS', 'True')
DEFAULT_FROM_EMAIL = 'webmaster@in-un.ru'
SERVER_EMAIL = 'webmaster@in-un.ru'
#########################
# Default Group Settings
#########################
# DEFAULT_ALLIANCE_GROUP - Default group alliance members are put in
# DEFAULT_BLUE_GROUP - Default group for blue members
#########################
DEFAULT_ALLIANCE_GROUP = os.environ.get('AA_DEFAULT_ALLIANCE_GROUP', 'AllianceMember')
DEFAULT_BLUE_GROUP = os.environ.get('AA_DEFAULT_BLUE_GROUP', 'BlueMember')

#########################
# Alliance Service Setup
#########################
# ENABLE_ALLIANCE_FORUM - Enable forum support in the auth for alliance members
# ENABLE_ALLIANCE_JABBER - Enable jabber support in the auth for alliance members
# ENABLE_ALLIANCE_MUMBLE - Enable mumble support in the auth for alliance members
# ENABLE_ALLIANCE_IPBOARD - Enable IPBoard forum support in the auth for alliance members
#########################
ENABLE_ALLIANCE_FORUM = 'False' # == os.environ.get('AA_ENABLE_ALLIANCE_FORUM', 'False')
ENABLE_ALLIANCE_JABBER = 'False'# == os.environ.get('AA_ENABLE_ALLIANCE_JABBER', 'False')
ENABLE_ALLIANCE_MUMBLE = 'False' #== os.environ.get('AA_ENABLE_ALLIANCE_MUMBLE', 'False')
ENABLE_ALLIANCE_IPBOARD = 'False' #== os.environ.get('AA_ENABLE_ALLIANCE_IPBOARD', 'False')
ENABLE_ALLIANCE_TEAMSPEAK3 = True #== os.environ.get('AA_ENABLE_ALLIANCE_TEAMSPEAK3', 'False')

#####################
# Blue service Setup
#####################
# ENABLE_BLUE_FORUM - Enable forum support in the auth for blues
# ENABLE_BLUE_JABBER - Enable jabber support in the auth for blues
# ENABLE_BLUE_MUMBLE - Enable mumble support in the auth for blues
# ENABLE_BLUE_IPBOARD - Enable IPBoard forum support in the auth for blues
#####################
ENABLE_BLUE_FORUM = 'False' # == os.environ.get('AA_ENABLE_BLUE_FORUM', 'False')
ENABLE_BLUE_JABBER = 'False' #== os.environ.get('AA_ENABLE_BLUE_JABBER', 'False')
ENABLE_BLUE_MUMBLE = 'False' #== os.environ.get('AA_ENABLE_BLUE_MUMBLE', 'False')
ENABLE_BLUE_IPBOARD = 'False' #== os.environ.get('AA_ENABLE_BLUE_IPBOARD', 'False')
ENABLE_BLUE_TEAMSPEAK3 = 'False' #== os.environ.get('AA_ENABLE_BLUE_TEAMSPEAK3', 'False')

#########################
# Alliance Configuration
#########################
# ALLIANCE_ID - Set this to your AllianceID 
# ALLIANCE_NAME - Set this to your Alliance Name
# ALLIANCE_EXEC_CORP_ID - Set this to the api id for the exec corp
# ALLIANCE_EXEC_CORP_VCODE - Set this to the api vcode for the exe corp
# ALLIANCE_BLUE_STANDING - The default lowest standings setting to consider blue
########################
ALLIANCE_ID = os.environ.get('AA_ALLIANCE_ID', '99005103')
ALLIANCE_NAME = os.environ.get('AA_ALLIANCE_NAME', 'Independent Union')
ALLIANCE_TIKER = os.environ.get('AA_ALLIANCE_NAME', 'IN-UN')
ALLIANCE_EXEC_CORP_ID = os.environ.get('AA_ALLIANCE_EXEC_CORP_ID', '4505846')
ALLIANCE_EXEC_CORP_VCODE = os.environ.get('AA_ALLIANCE_EXEC_CORP_VCODE', '0MOSFmlwnzqbO5LXdZSOtj6jxWwuLsbgLVeJChG85OhGHdriGtqz1DEteXbxYcww')
ALLIANCE_BLUE_STANDING = float(os.environ.get('AA_ALLIANCE_BLUE_STANDING', '5.0'))

#####################
# HR Configuration
#####################
# JACK_KNIFE_URL - Url for the audit page of API Jack knife
#                  Should seriously replace with your own.
#####################
JACK_KNIFE_URL = os.environ.get('AA_JACK_KNIFE_URL', 'http://ridetheclown.com/eveapi/audit.php')

#####################
# Forum Configuration
#####################
# FORUM_URL - Forum url location
# IPBOARD_ENDPOINT - Api endpoint if using ipboard
# IPBOARD_APIKEY - Api key to interact with ipboard
# IPBOARD_APIMODULE - Module for alliance auth *leave alone*
#####################
FORUM_URL = os.environ.get('AA_FORUM_URL', "http://forum.in-un.ru")
IPBOARD_ENDPOINT = os.environ.get('AA_IPBOARD_ENDPOINT', 'someaddress.com/interface/board/index.php')
IPBOARD_APIKEY = os.environ.get('AA_IPBOARD_APIKEY', 'somekeyhere')
IPBOARD_APIMODULE = 'aa'

######################
# Jabber Configuration
######################
# JABBER_URL - Jabber address url
# JABBER_PORT - Jabber service portal
# JABBER_SERVER - Jabber server url
# OPENFIRE_ADDRESS - Address of the openfire admin console
# OPENFIRE_SECRET_KEY - Openfire userservice secret key
# BROADCAST_USER - Broadcast user JID
# BROADCAST_USER_PASSWORD - Broadcast user password
######################
JABBER_URL = os.environ.get('AA_JABBER_URL', "in-un.ru")
JABBER_PORT = int(os.environ.get('AA_JABBER_PORT', '5223'))
JABBER_SERVER = os.environ.get('AA_JABBER_SERVER', "in-un.ru")
OPENFIRE_ADDRESS = os.environ.get('AA_OPENFIRE_ADDRESS', "http://in-un.ru:9090/")
OPENFIRE_SECRET_KEY = os.environ.get('AA_OPENFIRE_SECRET_KEY', "somekey")
BROADCAST_USER = os.environ.get('AA_BROADCAST_USER', "broadcast@") + JABBER_URL
BROADCAST_USER_PASSWORD = os.environ.get('AA_BROADCAST_USER_PASSWORD', "somepassword")

######################################
# Mumble Configuration
######################################
# MUMBLE_URL - Mumble server url
# MUMBLE_SERVER_ID - Mumble server id
######################################
MUMBLE_URL = os.environ.get('AA_MUMBLE_URL', "in-un.ru")
MUMBLE_SERVER_ID = int(os.environ.get('AA_MUMBLE_SERVER_ID', '1'))

######################################
# Teamspeak3 Configuration
######################################
# TEAMSPEAK3_SERVER_IP - Teamspeak3 server ip
# TEAMSPEAK3_SERVER_PORT - Teamspeak3 server port
# TEAMSPEAK3_SERVERQUERY_USER - Teamspeak3 serverquery username
# TEAMSPEAK3_SERVERQUERY_PASSWORD - Teamspeak3 serverquery password
# TEAMSPEAK3_VIRTUAL_SERVER - Virtual server id
# TEAMSPEAK3_AUTHED_GROUP_ID - Default authed group id
# TEAMSPEAK3_PUBLIC_URL - teamspeak3 public url used for link creation
######################################
TEAMSPEAK3_SERVER_IP = os.environ.get('AA_TEAMSPEAK3_SERVER_IP', '127.0.0.1')
TEAMSPEAK3_SERVER_PORT = int(os.environ.get('AA_TEAMSPEAK3_SERVER_PORT', '10011'))
TEAMSPEAK3_SERVERQUERY_USER = os.environ.get('AA_TEAMSPEAK3_SERVERQUERY_USER', 'serveradmin')
TEAMSPEAK3_SERVERQUERY_PASSWORD = os.environ.get('AA_TEAMSPEAK3_SERVERQUERY_PASSWORD', 'passwordhere')
TEAMSPEAK3_VIRTUAL_SERVER = int(os.environ.get('AA_TEAMSPEAK3_VIRTUAL_SERVER', '1'))
TEAMSPEAK3_PUBLIC_URL = os.environ.get('AA_TEAMSPEAK3_PUBLIC_URL', 'in-un.ru')





try:
    from local_settings import *
except ImportError:
    pass
