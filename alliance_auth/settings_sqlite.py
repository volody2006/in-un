# -*- coding: utf-8 -*-

__author__ = 'user'

"""
Конфигурация для запуска тестов

Томский будет конфигом по умолчанию, соответственно, все миграции, валидации и т.п.
без явного указания модуля конфигурации будут проходить c DEPARTMENT_ID = 1 (Томск) и
с GMT-зоной +6
"""

from alliance_auth.settings import *
import redis
import sys
import os
import requests

try:
    # Использование тест-раннера django-coverage
    # from django_coverage.coverage_runner import CoverageRunner as DjangoTestSuiteRunner
    import django_coverage
    USE_DJANGO_COVERAGE = True

except ImportError:
    # Использование стандартного тест-раннера из django
    USE_DJANGO_COVERAGE = False

# В качестве базы данных используем созданный в памяти SQLite
DATABASES['default'].update(**{
    'ENGINE': 'django.db.backends.sqlite3',
    'NAME':  ':memory:'
})




# Добавляем django.contrib.sites для запуска тестов
INSTALLED_APPS = (
    'django.contrib.sites',
                 ) + INSTALLED_APPS

# Приложения, которые не надо тестировать  (@TODO: сделать возможность использовать регулярки?)
# EXCLUDE_APPS = (
#     'django_extensions',
#     'pagination',
#     'social_auth',
#     'annoying',
#     'pytils',
# )


# Настройки redis для запуска тестов. Параметр REDIS_DB *ОБЯЗАТЕЛЬНО* должен отличаться от
# одноимённого параметра в основном конфиге
# REDIS_HOST = '127.0.0.1'
# REDIS_PORT = 6379
# REDIS_DB = 4
# REDIS_POOL = redis.ConnectionPool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)


# Эта переменная нужна для защиты запуск
IS_TEST_SETTINGS = True

# Не запускаем миграции во время тестов
SOUTH_TESTS_MIGRATE = False

# Не используем django-compressor во время тестов
COMPRESS_ENABLED = False

# затыкаем раввина
# RAVEN_CONFIG = {}

# Нужно ли выводить logging-сообщения в консоль?
LOGGING_SILENT = True

# Настройка логирования
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': u'[%(levelname)s] %(asctime)s [module=%(module)s PID=%(process)d] %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'django.utils.log.NullHandler',
            },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'filters': ['require_debug_false'],
        },
        # 'sentry': {
        #     'level': 'DEBUG',
        #     'class': 'raven.contrib.django.handlers.SentryHandler',
        # },
    },
    'loggers': {
        'django': {
            'handlers': ['null'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            'handlers': ['null'],
            'level': 'ERROR',
            'propagate': False,
        },
        '': {
            'level': 'DEBUG',
            'handlers': ['null' if LOGGING_SILENT else 'console'],
        }
    }
}

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
        'URL': 'http://poopiter.tomsk.fm/solr/solr_test',
        'EXCLUDED_INDEXES': ['thematic_extracts.search_indexes.DepThemeIndex'],
    },
    'theme_index': {
        'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
        'URL': 'http://poopiter.tomsk.fm/solr/solr_test2',
        'EXCLUDED_INDEXES': ['market.search_indexes.LotIndex'],
    },
}


# Принудительно очищаем солры
for name, data in HAYSTACK_CONNECTIONS.items():
    requests.get('%s/update?stream.body=<delete><query>*:*</query></delete>' % data['URL'])

# Принудительно очищаем редис
# redis.StrictRedis(connection_pool=REDIS_POOL).flushdb()


# Если есть установленный django-coverage, подменяем раннеры
if USE_DJANGO_COVERAGE:
    TEST_RUNNER = 'django_coverage.coverage_runner.CoverageRunner'
    COVERAGE_TEST_RUNNER = 'django_coverage.coverage_runner.CoverageRunner'

    COVERAGE_USE_CACHE = True

    # Регулярные выражения строк, которые будут пропускаться
    COVERAGE_CODE_EXCLUDES = [
        'raise NotImplemented',
        'from .* import .*', 'import .*',
    ]

    # Регулярные выражения путей (директорий), которые будут игнорироваться
    import social_auth
    COVERAGE_PATH_EXCLUDES = [
        r'.svn',
        social_auth.__path__[0],
    ]

    # Регуляное выражения для путей (директорий и файлов), которые не будут
    # анализироваться
    COVERAGE_MODULE_EXCLUDES = [
        'management.commands',
        'locale$',
        '^django\.',
        'migrations',
        '^celery_haystack\.',
        '^compressor\.',
        '^debug_toolbar\.',
        '^django_declension\.',
        '^django_config_gen\.',
        '^djcelery\.',
        '^djkombu\.',
        '^haystack\.',
        '^mptt\.',
        '^raven\.',
        '^registration\.',
        '^social_auth\.',
        '^sorl\.',
        '^south\.',
        '^tagging\.',
    ]

    # Директория, в которой будут сохраняться сгенерированные HTML-отчёты о покрытии
    COVERAGE_REPORT_HTML_OUTPUT_DIR = rel('.coverage_report') + '/'

    # Если директории нет, создадим
    if COVERAGE_REPORT_HTML_OUTPUT_DIR and not os.path.exists(COVERAGE_REPORT_HTML_OUTPUT_DIR):
        os.makedirs(COVERAGE_REPORT_HTML_OUTPUT_DIR)

    # True - всегда выводить отчёт о покрытии в STDOUT.
    COVERAGE_CUSTOM_REPORTS = True

    # Не только генерировать HTML-отчёт, но и выводить сводную статистику по покрытыю в STDOUT
    COVERAGE_USE_STDOUT = False

    # The name of the folder within utils/coverage_report/badges/ that
    # contains the badges we want to use.
    COVERAGE_BADGE_TYPE = 'drone.io'

else:
    print """

    WARNING! No coverage utilities found. It's optional, but highly
    recommended. To install it, just run these commands:

        $ pip install coverage
        $ pip install django-coverage

    Of course you can do it even with one command:

        $ pip install coverage django-coverage

    After it run this command and get coverage report into .coverage_report
    directory

    """
    try:
        raw_input('Press ENTER to continue or CTRL+C to abort')
    except KeyboardInterrupt:
        print "\nBye!\n"
        sys.exit(0)