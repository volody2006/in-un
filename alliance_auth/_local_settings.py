# -*- coding: utf-8 -*-
import os

__author__ = 'user'

DATABASES = {
#'default': {
#'ENGINE': 'django.db.backends.sqlite3',
#'NAME': 'development.sqlite3',
#'USER': '',
#'PASSWORD': '',
#'HOST': '',
#'PORT': '',
#}
    # GpuP2bY6DB3b9ccL

    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'test_inun',
        'USER': 'volody',
        'PASSWORD': 'GpuP2bY6DB3b9ccL',
        'HOST': 'localhost',
        'PORT': '3306',
    },

    'eve': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'universeDataDx.db',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    },

    'c1inun': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'old_inun',
        'USER': 'volody',
        'PASSWORD': 'GpuP2bY6DB3b9ccL',
        'HOST': 'localhost',
        'PORT': '3306',
    },
}

#INSTALLED_APPS += ("djcelery", )

DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']

# /home/user/in-un/static/adminlte/dist/js/app.min.js


API_ID_TEST = '1469805'
API_Code_TEST = 'hODuGtxHbvkErlCyFn51ffwFM9REubJ0cgJjcvWHdVYc3gtg3HxF6nAV2mhaLghu'
 #'http://community.testeveonline.com/support/api-key'

TEAMSPEAK3_SERVER_IP = os.environ.get('AA_TEAMSPEAK3_SERVER_IP', '127.0.0.1')
TEAMSPEAK3_SERVER_PORT = int(os.environ.get('AA_TEAMSPEAK3_SERVER_PORT', '10011'))
TEAMSPEAK3_SERVERQUERY_USER = os.environ.get('AA_TEAMSPEAK3_SERVERQUERY_USER', 'serveradmin')
TEAMSPEAK3_SERVERQUERY_PASSWORD = os.environ.get('AA_TEAMSPEAK3_SERVERQUERY_PASSWORD', 'zgiHZnYz')
TEAMSPEAK3_VIRTUAL_SERVER = int(os.environ.get('AA_TEAMSPEAK3_VIRTUAL_SERVER', '1'))
TEAMSPEAK3_PUBLIC_URL = os.environ.get('AA_TEAMSPEAK3_PUBLIC_URL', 'localhost')