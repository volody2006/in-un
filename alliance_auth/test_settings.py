# -*- coding: utf-8 -*-
#python manage.py test --settings=alliance_auth.test_settings
__author__ = 'user'
from settings import *
import os
#DATABASE_ENGINE = 'sqlite3'
#DATABASE_NAME = 'testdb.sqlite'
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': 'test.sqlite3',
#         'USER': '',
#         'PASSWORD': '',
#         'HOST': '',
#         'PORT': '',
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:'
    }
}

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.MD5PasswordHasher',
]

class DisableMigrations(object):

    def __contains__(self, item):
        return True

    def __getitem__(self, item):
        return "notmigrations"
#
MIGRATION_MODULES = DisableMigrations()