# Create your tests here.
# -*- coding: utf-8 -*-
import unittest
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import Client, TestCase


class RegistrationTest(TestCase):
  #  fixtures = ['data.json']
    fixtures = ['fixtures/eveonline_fix.json']
    def setUp(self):
        pass

    def test_user_reg_obj(self):
        User.objects.create_user('Test_user','email@test.com', 'password123')
        user= User.objects.get(username = 'Test_user')
        self.assertEqual(user.username, 'Test_user')



    def test_reg_user(self):
        c = Client()
        #Проходим регистрацию
        response = c.post(reverse('auth_register_user'),
                          {'username': 'Test_user',
                           'password': 'test123',
                           'password_again': 'test123',
                           'email': 'test_user@in-un.ru',
                           'email_again': 'test_user@in-un.ru', })
        self.assertEquals(response.status_code, 302)

        #Не проходим регистрацию, Юзер нейм уже занят
        response = c.post(reverse('auth_register_user'),
                          {'username': 'Test_user',
                           'password': 'test123',
                           'password_again': 'test123',
                           'email': 'test_user@in-un.ru',
                           'email_again': 'test_user@in-un.ru', })
        self.assertEquals(response.status_code, 200)

        #Не проходим регистрацию, Пароли не совпадают
        response = c.post(reverse('auth_register_user'),
                          {'username': 'Test_user',
                           'password': 'test123',
                           'password_again': '123',
                           'email': 'test_user@in-un.ru',
                           'email_again': 'test_user@in-un.ru', })
        self.assertEquals(response.status_code, 200)

        #Не проходим регистрацию, Пароли пустые
        response = c.post(reverse('auth_register_user'),
                          {'username': 'Test_user',
                           'password': '',
                           'password_again': '',
                           'email': 'test_user@in-un.ru',
                           'email_again': 'test_user@in-un.ru', })
        self.assertEquals(response.status_code, 200)

        #Не проходим регистрацию, Емайл адреса разные.
        response = c.post(reverse('auth_register_user'),
                          {'username': 'Test_user',
                           'password': 'test123',
                           'password_again': 'test123',
                           'email': 'test_user@in-un.ru',
                           'email_again': 'oser@in-un.ru', })
        self.assertEquals(response.status_code, 200)

        #Не можем залогиниться не существующим пользователем.
        response = c.post(reverse('auth_login_user'), {'username': 'Ozer', 'password': 'test123'})
        self.assertEquals(response.status_code, 200)

        #Успешный логин.
        response = c.post(reverse('auth_login_user'), {'username': 'Test_user', 'password': 'test123'})
        self.assertEquals(response.status_code, 302)

    #return HttpResponseRedirect(reverse('auth_api_key_management'))