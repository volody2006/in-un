# -*- coding: utf-8 -*-
from eveonline.managers import EveManager
from eveonline.models import EveCharacter
from wallet.models import CorpJournal

__author__ = 'user'

def extract_char():
    obj = CorpJournal.objects.filter(type_id=85)
    character = [x.party_2_id for x in obj]
    character = set(character)
    character = [x for x in character]
    for character_id in character:
        if not EveCharacter.objects.filter(character_id=character_id).exists():
            character = EveManager.character_info(character_id)
            print ('character %s' % character['name'])
        else:
            print('character yes')
