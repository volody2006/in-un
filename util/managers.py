# -*- coding: utf-8 -*-
import calendar
from datetime import timedelta, datetime
from django.utils import timezone
from django.utils.timezone import make_aware

__author__ = 'user'


def _start_date(period, start=timezone.now()):
    """
    Returns a date with a timedelta days_from_now and time 0:0:0 to mark the start of the day.
    """
    return start_of_day(start) - timedelta(period)

def end_of_day(datetime):
    return datetime.replace(hour=23, minute=59, second=59)


def start_of_day(datetime):
    return datetime.replace(hour=0, minute=0, second=0)


def month_period(period=None):
    to_date = timezone.now()
    if period == 0:
        from_date = to_date.replace(day=1, hour=0, minute=0, second=0, microsecond = 0)
        return from_date, to_date
    else:

        now_mon = to_date.month
        last_mon = now_mon - period
        year = to_date.year
        last_day = calendar.monthrange(year, last_mon)
        from_date = to_date.replace(month=last_mon, day=1, hour=0, minute=0, second=0, microsecond = 0)
        to_date = to_date.replace(month=last_mon, day=last_day[1], hour=23, minute=59, second=59, microsecond = 999999)
        return from_date, to_date